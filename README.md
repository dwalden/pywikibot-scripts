# pywikibot scripts

Scripts I have written for pywikibot.

How to use these scripts
========================

1. Setup pywikibot using the instructions here https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
2. Copy the scripts in the `scripts/` directory of this repository into the `scripts/` directory in pywikibot

Some of the scripts will have documentation in the source file itself. Others will not, but hopefully you can work out the supported parameters by reading the `set_options()` function.

Testing beta and local wikis
====================

To be able to use pywikibot with beta wiki sites, you need to generate them:
```
python3 pwb.py generate_family_file https://en.wikipedia.beta.wmflabs.org betawikipedia y
python3 pwb.py generate_family_file https://en.wikibooks.beta.wmflabs.org betawikibooks y
python3 pwb.py generate_family_file https://en.wiktionary.beta.wmflabs.org betawiktionary y
python3 pwb.py generate_family_file https://en.wikinews.beta.wmflabs.org betawikinews y
python3 pwb.py generate_family_file https://en.wikiquote.beta.wmflabs.org betawikiquote y
python3 pwb.py generate_family_file https://en.wikisource.beta.wmflabs.org betawikisource y
python3 pwb.py generate_family_file https://en.wikiversity.beta.wmflabs.org betawikiversity y
python3 pwb.py generate_family_file https://en.wikivoyage.beta.wmflabs.org betawikivoyage y
python3 pwb.py generate_family_file https://commons.wikimedia.beta.wmflabs.org betacommons n
python3 pwb.py generate_family_file https://wikidata.beta.wmflabs.org betawikidata n
python3 pwb.py generate_family_file https://wikifunctions.beta.wmflabs.org betawikifunctions n
```

You will want to add the username for beta site you want to test to `user-config.py`, such as:
```
usernames['betawikipedia']['en'] = u'<username>'
```

Then, for example, to run any of the scripts against https://en.wikipedia.beta.wmflabs.org you can run: `python3 pwb.py <script> <arguments> -lang:en -family:betawikipedia`.

To be able to use pywikibot against your local docker wiki, you need to generate them:
```
python3 pwb.py generate_family_file http://localhost:8080 dockerwiki n
```

Add this to `user-config.py` ('Admin' is the default docker admin user):
```
usernames['dockerwiki']['en'] = u'Admin'
```

Then to run any of the scripts against your local docker wiki you can run: `python3 pwb.py <script> <arguments> -lang:en -family:dockerwiki`.

`all_params_api_db.py`
====================

1. Setup a local MediaWiki install (the subsequent steps assume you have followed https://gerrit.wikimedia.org/g/mediawiki/core/+/HEAD/DEVELOPERS.md)
2. To `LocalSettings.php`, add `$wgDebugToolbar = true;`
3. Checkout pywikibot: `git clone --recursive --branch stable https://gerrit.wikimedia.org/r/pywikibot/core.git pywikibot`
4. `cd pywikibot`
5. Install pywikibot dependencies: `python3 -m pip install "requests>=2.20.1" "mwparserfromhell>=0.5.2" packaging`
6. Tell pywikibot about your local wiki: `python3 pwb.py generate_family_file http://localhost:8080 localwiki n`
7. In the pywikibot directory, create `user-config.py` containing `usernames['localwiki']['en'] = 'Admin'`
8. Copy `all_params_api_db.py` into pywikibot's `scripts` directory
9. Decide which action API module or action you want to test (see https://www.mediawiki.org/wiki/Special:MyLanguage/API:Main_page)
10. Work out what the module's name is. Mostly, this is just the name of the action e.g. `edit`, `delete`. For the `query` action, find the name of the `prop`, `list` or `meta` you want, then it would be `query+<name>`
10. Construct an SQL query, with columns for each API parameter you want to test and a row for every request you want to send. This is probably the hardest part to work out. Find some guidance here: https://www.mediawiki.org/wiki/User:DWalden_(WMF)/Test_data_sampling_using_SQL
11. Run the script: `python3 pwb.py all_params_api_db -m <module/action name> -q "<query>" -d /path/to/my_wiki.sqlite -lang:en -family:localwiki`

For example, `python3 pwb.py all_params_api_db -m query+allrevisions -q "SELECT actor_name AS 'arvuser' FROM actor" -d ~/core/cache/sqlite/my_wiki.sqlite -lang:en -family:localwiki`.

It will write a CSV file named like `all_params_api_db_<wikiname>_<date>.csv` in the current directory. This contains the performance data for each request. Each row is a single request and the columns are for the request parameters, the SQL queries ran and how long the queries took in seconds.
