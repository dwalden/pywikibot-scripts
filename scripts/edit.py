#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Edit or unwatch a page.

The following parameters are supported:

-t                List of Titles, separated by |.
-p                List of Page IDs, separated by |.
-r                List of Revision IDs, separated by |.
-u                Unwatch targets.
-e                Expiry time. May be relative (e.g. 5 months or 2 weeks) or absolute (e.g. 2014-09-18T12:34:56Z). If set to infinite, indefinite, or never, the watch will never expire.
-d                Automatically resolve redirects.
-c                Convert titles to other variants if necessary and supported.
-g                "Get the list of pages to work on by executing the specified query module."
--user            User to login as.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class Edit(object):

    """Edit or unwatch a page."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--title', required=True, help="Page title.")
        parser.add_argument('-e', '--expiry', default="", help="Expiry time. May be relative (e.g. 5 months or 2 weeks) or absolute (e.g. 2014-09-18T12:34:56Z), or infinite, indefinite, or never.")
        parser.add_argument('-w', '--watchlist', default="preferences", help="Unwatch targets.")
        parser.add_argument('-a', '--append', default="", help="Custom arguments. Specified like: 'arg1=val1&arg2=val2|val3'")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        self.site.throttle.setDelays(0, 0, True)
        params = {'action': 'edit',
                  'title': self.options.title,
                  'appendtext': self.options.append,
                  'watchlist': self.options.watchlist,
                  'format': 'json',
                  'token': self.site.get_tokens(["csrf"])["csrf"]}

        if self.options.expiry:
            params['watchlistexpiry'] = self.options.expiry

        r = Request(site=self.site, parameters=params, append_headers={'X-Forwarded-For': '1.2.3.4'})
        try:
            response = r.submit()
            return response
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Edit(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
