#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class CreateAccount(object):

    """Send an email to a user."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-u', '--user', default=False)
        parser.add_argument('-e', '--email', default=False)
        parser.add_argument('-l', '--login', action='store_true')
        parser.add_argument('-c', '--capture', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        if self.options.login:
            self.site.login()

        params = {'action': 'resetpassword',
                  'format': 'json',
                  'token': self.site.tokens['csrf']}
        if self.options.user:
            params['user'] = self.options.user
        if self.options.email:
            params['email'] = self.options.email
        if self.options.capture:
            params['capture'] = 1

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            pywikibot.output(response)
            return response
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = CreateAccount(*args)
    app.run()


if __name__ == '__main__':
    main()
