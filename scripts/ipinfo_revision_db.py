#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import csv
from datetime import datetime
import requests
import copy

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError
from pywikibot.data.mysql import mysql_query
from pywikibot.comms import http

class IPInfoRevisionDB(object):

    """IPInfoRevisionDB user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=1)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        outputfile = "ipinfo_revision_{}_{}_{}_{}.csv".format(self.site.code, self.site.family, self.site.user(), datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))

        rows = []
        fields = ["rev_id", "Editor Hidden", "Revision Suppressed", "Editor", "Editor Is IP", "Editor Suppressed", "Response"]

#         query = """
# SELECT rev_id,
#        IF(4 & rev_deleted, "Yes", "No") AS "Editor Hidden",
#        IF(8 & rev_deleted, "Yes", "No") AS "Revision Suppressed",
#        actor_name AS Editor,
#        IF(actor_name REGEXP "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|^[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+$", "Yes", "No") AS "Editor Is IP",
#        IF(editor_block.ipb_deleted, "Yes", "No") AS "Editor Suppressed"
# FROM revision
# INNER JOIN revision_actor_temp ON revision.rev_id = revision_actor_temp.revactor_rev
# INNER JOIN actor ON revision_actor_temp.revactor_actor = actor.actor_id
# LEFT JOIN ipblocks AS editor_block ON actor.actor_user = editor_block.ipb_user
# GROUP BY `Editor Suppressed`, `Editor Is IP`, `Editor Hidden`, `Revision Suppressed`;
# """
        query = """
WITH random_revisions AS
(SELECT foo.*, row_number() over(partition by `Editor Suppressed`, `Editor Is IP`, `Editor Hidden`, `Revision Suppressed` order by rand()) as random_sort FROM
(SELECT rev_id,
       IF(4 & rev_deleted, "Yes", "No") AS "Editor Hidden",
       IF(8 & rev_deleted, "Yes", "No") AS "Revision Suppressed",
       actor_name AS Editor,
       IF(actor_name REGEXP "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|^[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+$", "Yes", "No") AS "Editor Is IP",
       IF(editor_block.ipb_deleted, "Yes", "No") AS "Editor Suppressed"
FROM revision
INNER JOIN revision_actor_temp ON revision.rev_id = revision_actor_temp.revactor_rev
INNER JOIN actor ON revision_actor_temp.revactor_actor = actor.actor_id
LEFT JOIN ipblocks AS editor_block ON actor.actor_user = editor_block.ipb_user) AS foo)
SELECT * FROM random_revisions WHERE random_sort <= {};
""".format(self.options.total)

        revisions = mysql_query(query, dbname=self.site.dbName())

        for revisionentry in revisions:
            # Makes a request for logged in user
            response = http.session.get("http://{}.wiki.local.wmftest.net:8080/w/rest.php/ipinfo/v0/revision/{}".format(self.site.code, revisionentry['rev_id'])).json()

            row = revisionentry

            if 'messageTranslations' in response:
                row['Response'] = response['messageTranslations']['en']
            elif 'info' in response:
                row['Response'] = []
                for ipinfo in response['info']:
                    row['Response'].append(ipinfo['subject'])

            rows.append(row)

        with open(outputfile, 'a', newline='') as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=False, extrasaction="ignore")
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = IPInfoRevisionDB(*args)
    app.run()


if __name__ == '__main__':
    main()

