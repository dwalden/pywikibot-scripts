#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import ctypes
import requests

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


c_uint8 = ctypes.c_uint8

class Flags_bits(ctypes.LittleEndianStructure):
    _fields_ = [
        ("DELETED_TEXT", c_uint8, 1),  # asByte & 1 
        ("DELETED_COMMENT", c_uint8, 1),  # asByte & 2
        ("DELETED_USER", c_uint8, 1),  # asByte & 4
        ("DELETED_RESTRICTED", c_uint8, 1),  # asByte & 8
        ("SUPPRESSED_USER", c_uint8, 1),  # asByte & 16
    ]

class Flags(ctypes.Union):
    _anonymous_ = ("bit",)
    _fields_ = [
        ("bit", Flags_bits),
        ("asInt", c_uint8)
    ]

class ListDeleteRevisions(object):

    """Block user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        self.args = args
        my_args = pywikibot.handle_args(args)
        # parser = argparse.ArgumentParser()
        # self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        request = Request(site=self.site, parameters={'action': 'query',
                                                      'list': 'logevents',
                                                      'letype': 'delete',
                                                      'leaction': 'delete/revision',
                                                      'lenamespace': 0,
                                                      'lelimit': 500})
        try:
            response = request.submit()
            logevents = response['query']['logevents']

            for logevent in logevents:
                flags = Flags()
                flags.asInt = logevent['params']['new']['bitmask']

                if int(flags.bit.DELETED_USER) == 1:
                    s = requests.session()
                    r = s.get("https://wikiwho.wmflabs.org/{}/whocolor/v1.0.0-beta/{}/{}/".format("en", logevent['title'], logevent['params']['ids'][0]))
                    if r.status_code == 200:
                        r = r.json()

                    if 'revisions' in r and r['revisions'][str(logevent['params']['ids'][0])][3] != "":
                        pywikibot.output("Hidden: {} {}".format(logevent['title'], logevent['params']['ids'][0]))

        except APIError as err:
            pywikibot.output(err)

def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = ListDeleteRevisions(*args)
    app.run()


if __name__ == '__main__':
    main()


