#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import argparse
import pywikibot
import sqlite3
import re

from pywikibot.comms import http


class TempAccAllUsers(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-s', '--sqlite', required=True, default=False)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        query = "SELECT actor_name, GROUP_CONCAT(DISTINCT cuc_ip) FROM (SELECT actor_name, cuc_ip FROM cu_changes LEFT JOIN actor ON cuc_actor=actor_id ORDER BY cuc_ip DESC) GROUP BY actor_name;"
        conn = sqlite3.connect(self.options.sqlite)
        conn.create_function('regexp', 2, lambda x, y: 1 if re.search(x, y) else 0)
        cursor = conn.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        conn.close()
        self.site.login()
        for row in rows:
            name = row[0]
            ips = row[1].split(",")
            response = http.session.get("{}://{}{}/rest.php/checkuser/v0/temporaryaccount/{}".format(self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), name)).json()
            result_string = ""
            if 'ips' in response:
                response_ips = response['ips']
                result_string = "{} list?: {}".format(name, type(response_ips) == list)
                result_string += ". Match?: {} ({} vs. {})".format(response_ips == ips, len(response_ips), len(ips))
                if type(response_ips) == list:
                    response_ips.sort()
                    ips.sort()
                    result_string += ". Sorted match?: {} ({} vs. {})".format(response_ips == ips, len(response_ips), len(ips))
            else:
                result_string = "{}: {}".format(name, response)
            pywikibot.output(result_string)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = TempAccAllUsers(*args)
    app.run()
    # sys.stdout.writelines(app.run())
    # print(app.run())


if __name__ == '__main__':
    main()
