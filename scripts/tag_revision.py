#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Tag a revision, recent change or log entry.

The following parameters are supported:

-r                Revision ID
-c                Recent Change ID
-l                Log ID
-t                Tags
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals

import argparse
import os
import sys
import tempfile

import pywikibot

from pywikibot.data.api import Request, APIError


class TagRevision(object):

    """Edit a wiki page."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)

        parser = argparse.ArgumentParser()
        parser.add_argument('-r', '--revision')
        parser.add_argument('-c', '--rcid')
        parser.add_argument('-l', '--logid')
        parser.add_argument('-t', '--tags', required=True)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        params = {"action":"tag",
                  "add":self.options.tags,
                  "token":self.site.get_tokens(["csrf"])["csrf"]}
        if self.options.revision != "":
            params["revid"] = self.options.revision
        if self.options.rcid != "":
            params["rcid"] = self.options.rcid
        if self.options.logid != "":
            params["logid"] = self.options.logid

        r = Request(site=self.site,
                    parameters=params)
        try:
            response = r.submit()
            pywikibot.output(response)
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = TagRevision(*args)
    app.run()


if __name__ == '__main__':
    main()
