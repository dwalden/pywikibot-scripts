#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Investigate user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import pywikibot
import ipaddress
import csv
import urllib

from bs4 import BeautifulSoup

from pywikibot.comms.http import request
from pywikibot.data.mysql import mysql_query

class SpecialSecurePollLog(object):

    """Submit Special:Investigate"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        with open("foo.csv", 'r', newline='') as csvfile:
            dictcsv = csv.DictReader(csvfile)

            for comb in dictcsv:
                search = {'type': comb['Type']}

                if comb['Election']:
                    search['election_name'] = comb['Election']
                if comb['Performer']:
                    search['performer'] = comb['Performer']
                if comb['Target']:
                    search['target'] = comb['Target']
                if comb['Date']:
                    search['date'] = comb['DateBefore']
                if comb['Action']:
                    search['action'] = comb['ActionId']

                response = request(site=self.site, uri="wiki/Special:SecurePollLog", method='GET', params=search).text

                soup = BeautifulSoup(response)
                div = soup.find("div", id="mw-content-text")
                lines = div.find_all("li")

                expected = comb['Expected'].split("|")
                expected.reverse()
                for line in lines:
                    i = lines.index(line)
                    if (expected[i] != line.get_text()):
                        pywikibot.output(search)
                        pywikibot.output(line.get_text())
                        pywikibot.output(expected[i])


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = SpecialSecurePollLog(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
