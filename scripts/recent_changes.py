#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
List recent changes.

The following parameters are supported:

TODO
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals

import argparse
import os
import sys
import tempfile

import pywikibot

from pywikibot.data.api import Request, APIError


class RecentChanges(object):

    """List Recent Changes"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-s', '--start', default="")
        parser.add_argument('-e', '--end', default="")
        parser.add_argument('-d', '--dir', default="")
        parser.add_argument('-n', '--namespace', default="")
        parser.add_argument('-u', '--user', default="")
        parser.add_argument('-x', '--excludeuser', default="")
        parser.add_argument('-t', '--tag', default="")
        parser.add_argument('-p', '--prop', default="")
        parser.add_argument('-w', '--show', default="")
        parser.add_argument('-l', '--limit', default="")
        parser.add_argument('-y', '--type', default="")
        parser.add_argument('-o', '--toponly', default="")
        parser.add_argument('-i', '--title', default="")
        parser.add_argument('-r', '--generaterevisions', default="")
        parser.add_argument('--slot', default="")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        params = {'action': 'query',
                  'list': 'recentchanges',
                  'format': 'json'}

        prefix = "rc"
        for key, value in vars(self.options).items():
            if value:
                params["rc{}".format(key)] = value

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            return response
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = RecentChanges(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
