#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Rollback or unwatch a page.

The following parameters are supported:

-t                List of Titles, separated by |.
-p                List of Page IDs, separated by |.
-r                List of Revision IDs, separated by |.
-u                Unwatch targets.
-e                Expiry time. May be relative (e.g. 5 months or 2 weeks) or absolute (e.g. 2014-09-18T12:34:56Z). If set to infinite, indefinite, or never, the watch will never expire.
-d                Automatically resolve redirects.
-c                Convert titles to other variants if necessary and supported.
-g                "Get the list of pages to work on by executing the specified query module."
--user            User to login as.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class Rollback(object):

    """Rollback or unwatch a page."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--title', default="", help="List of Titles, separated by |.")
        parser.add_argument('-e', '--expiry', default="", help="Expiry time. May be relative (e.g. 5 months or 2 weeks) or absolute (e.g. 2014-09-18T12:34:56Z), or infinite, indefinite, or never.")
        parser.add_argument('-w', '--watchlist', default="preferences", help="Unwatch targets.")
        parser.add_argument('-u', '--user', default="", help="Unwatch targets.")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        params = {'action': 'rollback',
                  'title': self.options.title,
                  'watchlist': self.options.watchlist,
                  'user': self.options.user,
                  'format': 'json',
                  'token': self.site.get_tokens(["rollback"])["rollback"]}

        if self.options.expiry:
            params['watchlistexpiry'] = self.options.expiry

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            return response
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Rollback(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
