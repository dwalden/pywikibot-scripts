#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Delete a revision or revisions.

The following parameters are supported:

-t                Type
-i                Revision ids
-t                Tags
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals

import argparse
import os
import sys
import tempfile

import pywikibot

from pywikibot.data.api import Request, APIError


class DeleteRevision(object):

    """Delete revision(s)."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)

        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--type', default="revision")
        parser.add_argument('--hide', default="")
        parser.add_argument('--show', default="")
        parser.add_argument('--suppress', default="nochange")
        parser.add_argument('-i', '--ids', required=True)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        params = {"action": "revisiondelete",
                  "type": self.options.type,
                  "ids": self.options.ids,
                  "suppress": self.options.suppress,
                  "token": self.site.get_tokens(["csrf"])["csrf"]}
        if self.options.hide != "":
            params['hide'] = self.options.hide
        if self.options.show != "":
            params['show'] = self.options.show

        r = Request(site=self.site,
                    parameters=params)
        try:
            response = r.submit()
            pywikibot.output(response)
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = DeleteRevision(*args)
    app.run()


if __name__ == '__main__':
    main()
