#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Tag a revision, recent change or log entry.

The following parameters are supported:

-p                Properties (as per "rcprop" in https://www.mediawiki.org/wiki/API:RecentChanges)
-n                Number of recent changes to return
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class PagesNotInNPPQueue(object):

    """List Recent Changes"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-n', '--namespaces', default=None)
        parser.add_argument('-t', '--total', default=10)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        rc = self.site._generator(api.ListGenerator, type_arg='random',
                                  namespaces=self.options.namespaces,
                                  total=self.options.total)
        for change in rc:
            r = Request(site=self.site, parameters={'action': 'pagetriagelist',
                                                    'page_id': change['id']})
            response = r.submit()
            pywikibot.output(change['title'])
            pywikibot.output(response['pagetriagelist']['result'])


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = PagesNotInNPPQueue(*args)
    app.run()


if __name__ == '__main__':
    main()
