#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import requests

import pywikibot
from pywikibot.page import User

class UnblockAll(object):

    def __init__(self, *args):
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        self.site.login()
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)

        all_blocks = self.site.blocks()

        for block in all_blocks:
            self.site.unblockuser(User(self.site, block['user']))


def main(*args):
    app = UnblockAll(*args)
    app.run()


if __name__ == '__main__':
    main()

