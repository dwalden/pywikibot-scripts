#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2022 Dominic Walden
# Copyright (c) 2003-2021 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# python3 pwb.py font_sizes -lang:<lang> -family:<site>
# e.g. to do 10 pages for each namespace:
#        python3 pwb.py font_sizes -n10 -lang:en -family:betawikipedia
# or to login when viewing the preview:
#        python3 pwb.py font_sizes -l -lang:en -family:betawikipedia

# If you want the selenium user to login, you probably want a user who is in
# the 'interface admin' group so they can edit Modules, JS, etc.


import argparse
import pywikibot

from pywikibot.data import api

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


class FontSizes(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-n', '--num', default=5, help="Number of pages to test per namespace")
        parser.add_argument('-l', '--login', action='store_true', help="Should the test user login before attempting to edit?")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        browser = webdriver.Chrome()
        if self.options.login:
            browser.get("{}://{}/wiki/Special:UserLogin".format(self.site.protocol(),
                                                                self.site.family.langs[self.site.code]))
            browser.find_element_by_id('wpName1').send_keys(self.site.username())
            login_manager = api.LoginManager(site=self.site, user=self.site.username())
            browser.find_element_by_id('wpPassword1').send_keys(login_manager.password)
            browser.find_element_by_id('wpLoginAttempt').click()
        params = {
            'action': 'query',
            'format': 'json',
            'meta': 'siteinfo',
            'siprop': 'namespaces'
        }
        req = self.site._request(parameters=params)
        namespaces = req.submit()
        for namespace in namespaces['query']['namespaces']:
            if int(namespace) >= 0:
                params = {
                    'action': 'query',
                    'format': 'json',
                    'list': 'random',
                    'rnlimit': self.options.num,
                    'rnnamespace': namespace
                }
                req = self.site._request(parameters=params)
                pages = req.submit()
                if len(pages['query']['random']) == 0:
                    print("{}: has no pages".format(namespace))
                    continue
                for page in pages['query']['random']:
                    browser.get("{}://{}/wiki/{}?action=edit".format(self.site.protocol(),
                                                                     self.site.family.langs[self.site.code],
                                                                     page['title']))
                    errors = ""
                    try:
                        browser.find_element_by_id('wpTextbox1').send_keys('foobar')
                    except Exception as err:
                        errors += str(err)
                    try:
                        browser.find_element(By.CLASS_NAME, 'ace_text-input').send_keys('foobar')
                    except Exception as err:
                        errors += str(err)
                    try:
                        browser.find_element_by_id('wpPreview').click()
                    except Exception as err:
                        errors += str(err)
                    try:
                        p = browser.find_element(By.CSS_SELECTOR, '.mw-parser-output p')
                        print("{} {}: {}".format(namespace, page['title'], p.value_of_css_property('font-size') == '14px'))
                        continue
                    except Exception as err:
                        errors += str(err)
                    try:
                        pre = browser.find_element(By.CSS_SELECTOR, '.mw-parser-output pre')
                        print("{} {}: {}".format(namespace, page['title'], pre.value_of_css_property('font-size') == '14px'))
                        continue
                    except Exception as err:
                        errors += str(err)
                    try:
                        td = browser.find_element(By.CSS_SELECTOR, '.mw-parser-output td')
                        print("{} {}: {}".format(namespace, page['title'], td.value_of_css_property('font-size') == '14px'))
                        continue
                    except Exception as err:
                        errors += str(err)
                    print("{}: Problem with {}. {}".format(namespace, page['title'], errors))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = FontSizes(*args)
    app.run()


if __name__ == '__main__':
    main()
