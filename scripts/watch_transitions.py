#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Watch or unwatch a page.

The following parameters are supported:
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import pymysql

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError

from .watch import Watch

from .deletepage import Delete
from .undeletepage import Undelete
from .edit import Edit
from .move import Move
from .protectpage import Protect
from .rollback import Rollback
# from .upload import Upload

class WatchTransitions(object):

    """Watch or unwatch a page."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()
        # self.site.throttle.setDelays(2, 2)

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        end_pages = {'perm': {},
                     'temp': {},
                     'unwatch': {}}
        for state_to in ["watch", "unwatch", "preferences", "nochange"]: # ["watch", "watch|1 month", "watch|infinite", "unwatch", "preferences", "preferences|2 weeks", "preferences|infinite", "nochange"]:
            end_pages["perm"][state_to] = {}
            end_pages["temp"][state_to] = {}
            end_pages["unwatch"][state_to] = {}
            foo = state_to.split("|")
            to_expiry_params = ["-w", foo[0]]
            if len(foo) > 1:
                to_expiry_params.extend(["-e", foo[1]])

            for action in [lambda page : Edit("-t", page, "-a", "Foobar", *to_expiry_params),
                           lambda page : Protect("-t", page, "-p", "edit=sysop", *to_expiry_params),
                           lambda page : Move("-f", page, "-t", "{}_moved".format(page), *to_expiry_params),
                           lambda page : Delete("-t", page, *to_expiry_params)]:

                perm_page = Watch("-g", "random", "-m", "grnlimit=1&grnnamespace=0").run()['watch'][0]['title']
                # temp_page = Watch("-g", "random", "-m", "grnlimit=1&grnnamespace=0", "-e", "1 week").run()['watch'][0]['title']
                unwatched_page = list(self.site._generator(api.ListGenerator, type_arg='random', namespaces=0, total=1))[0]['title']
                action(perm_page).run()
                # action(temp_page).run()
                action(unwatched_page).run()
                action_name = type(action("foo")).__name__
                end_pages["perm"][state_to][action_name] = perm_page
                # end_pages["temp"][state_to][action_name] = temp_page
                end_pages["unwatch"][state_to][action_name] = unwatched_page

            # Rollback specific
            for state_from, params in {"perm": "watch",
                                       "unwatch": "unwatch"}.items():
                foo = params.split("|")
                from_expiry_params = ["-w", foo[0]]
                if len(foo) > 1:
                    expiry_params.extend(["-e", foo[1]])
                pages = list(self.site._generator(api.ListGenerator, type_arg='random', namespaces=0, total=2))
                rollback_page = pages[0]['title']
                undelete_page = pages[1]['title']
                Edit("-t", rollback_page, "-a", "Foobar", *from_expiry_params).run()
                Rollback("-t", rollback_page, "-u", self.site.username(), *to_expiry_params).run()
                Delete("-t", undelete_page, *from_expiry_params).run()
                Undelete("-t", undelete_page, *to_expiry_params).run()
                end_pages[state_from][state_to]["Rollback"] = rollback_page
                end_pages[state_from][state_to]["Undelete"] = undelete_page

        not_first = False
        print("SELECT * FROM")
        print("(")
        for from_state, transitions in end_pages.items():
            for to_state, actions in transitions.items():
                for action, page in actions.items():
                    namespacep = page.split(":")
                    if len(namespacep) > 1:
                        page_name = namespacep[1]
                    else:
                        page_name = namespacep[0]

                    if not_first:
                        print("UNION")

                    print("SELECT '{}' AS from_state, '{}' AS to_state, '{}' AS action, wl_namespace, wl_title, we_expiry FROM watchlist LEFT JOIN watchlist_expiry ON wl_id=we_item WHERE wl_title IN ('{}', '{}_moved') AND wl_user={}".format(from_state, to_state, action, page_name.replace(" ", "_"), page_name.replace(" ", "_"), self.site.getuserinfo()['id']))

                    not_first = True

        print(") AS foo;")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = WatchTransitions(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
