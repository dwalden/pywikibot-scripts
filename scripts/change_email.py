#!/usr/bin/python
# -*- coding: utf-8 -*-
"""

"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError
from pywikibot.comms.http import request

from bs4 import BeautifulSoup

from .options import Options

class ChangeEmail(object):

    """ """

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        users = ["Drwpb1",
                 "Drwpb2",
                 "Drwpb4",
                 "Drwpb6",
                 "Drwpr",
                 "Drwpr1",
                 "Drwpr2",
                 "Drwpr3",
                 "Drwpww",
                 "Drwpww1",
                 "Drwpww2"]

        for user in users:
            self.site = pywikibot.Site(user=user)
            self.site.login()

            response = request(site=self.site, uri="wiki/Special:ChangeEmail", method='GET')
            soup = BeautifulSoup(response)
            edit_token = soup.find("input", id="wpEditToken")["value"]

            params = {"wpNewEmail": "domwalden4@gmail.com",
                      "wpEditToken": edit_token,
                      "title": "Special:ChangeEmail"}
            response = request(site=self.site, uri="wiki/Special:ChangeEmail", method='POST', params=params)
            # pywikibot.output(response)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = ChangeEmail(*args)
    app.run()


if __name__ == '__main__':
    main()
