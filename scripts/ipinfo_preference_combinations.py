#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import csv
import requests
from itertools import product

import pywikibot
from pywikibot.comms import http

from options import Options
from global_preferences import GlobalPreferences

class IPInfoPreferenceCombinations(object):

    def __init__(self, *args):
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-o', '--output', type=str, required=True)
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        self.site.login()
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)

        outcomes = []

        ipinfo_beta_feature_enable = list(product(["ipinfo-beta-feature-enable"], ["n/a", 0, 1], ["n/a", 0, 1], ["n/a", 0, 1]))
        ipinfo_enable = list(product(['ipinfo-enable'], ["n/a", 0, 1], ["n/a", 0, 1], ["n/a", 0, 1]))
        ipinfo_use_agreement = list(product(['ipinfo-use-agreement'], ["n/a", 0, 1], ["n/a", 0, 1], ["n/a", 0, 1]))
        combinations = product(ipinfo_beta_feature_enable, ipinfo_enable, ipinfo_use_agreement)

        for combination in combinations:

            change = ""
            gpchange = ""
            gplochange = ""

            for preference in combination:

                pref = preference[0]

                if preference[1] != "n/a":
                    if gpchange != "":
                        gpchange += "|"
                    gpchange += "{}={}".format(pref, preference[1])

                if preference[3] != "n/a":
                    if gplochange != "":
                        gplochange += "|"
                    gplochange += "{}={}".format(pref, preference[3])

                if preference[2] != "n/a":
                    if change != "":
                        change += "|"
                    change += "{}={}".format(pref, preference[2])

            if change != "":
                opt = Options("-c", change)
                opt.run()

            if gpchange != "":
                gp = GlobalPreferences("-c", gpchange)
                gp.run()

            if gplochange != "":
                gp = GlobalPreferences("-l", "-c", gplochange)
                gp.run()

            response = http.session.get("https://en.wikipedia.beta.wmflabs.org/w/rest.php/ipinfo/v0/revision/{}".format(94782)).json()
            if 'messageTranslations' in response:
                api_code = response['httpCode']
                api_reason = response['httpReason']
                api_message = response['messageTranslations']['en']
            else:
                api_code = 200
                api_reason = "success"
                api_message = "success"

            case = list(sum(combination, ()))
            case.append(api_code)
            case.append(api_reason)
            case.append(api_message)
            outcomes.append(case)

        with open(self.options.output, 'a', newline='') as outputcsv:
            outputfields = ['ipinfo-beta-feature-enable', 'global', 'local', 'override', 'ipinfo-enable', 'global', 'local', 'override', 'ipinfo-use-agreement', 'global', 'local', 'override', 'api_code', 'api_reason', 'api_message']
            writer = csv.DictWriter(outputcsv, outputfields, restval=False, extrasaction='ignore')
            writer.writeheader()
            writer.writerows(outcomes)


def main(*args):
    app = IPInfoPreferenceCombinations(*args)
    app.run()


if __name__ == '__main__':
    main()

