#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import json
import itertools
from datetime import datetime

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError
from pywikibot.data.mysql import mysql_query

from .specialblock import SpecialBlock
from .unblock import Unblock

class SpecialBlockSystematic(object):

    """SpecialBlockSystematic user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('--target', required=True)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        # self.site.throttle.setDelays(0, 0)
        outputfile = "{}_{}_{}_specialblock.json".format(self.site.code, self.site.family, datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))

        self.site.login()

        target = self.options.target

        editing = ["0", "1"]
        restriction = ["sitewide", "partial"]
        pagerestrictions = [False, "Foobar"]
        namespacerestrictions = [False, "3"]
        createaccount = ["1"]
        disableemail = ["0"]
        disableutedit = ["0", "1"]
        expiry = ["other"]
        expiryother = ["2020-10-01T07:46:01Z"]
        reason = ["other"]
        reasonother = [""]
        autoblock = ["0"]
        redirect = [""]
        previoustarget = [""]
        confirm = ["0", "1"]
        hideuser = ["0", "1"]
        # TODO: anononly

        final_results = []

        for case in itertools.product(editing, restriction, pagerestrictions, namespacerestrictions, createaccount, disableemail, disableutedit, expiry, expiryother, reason, reasonother, autoblock, redirect, previoustarget, confirm, hideuser):

            params = ["--editing", case[0], "--restriction", case[1], "--createaccount", case[4], "--disableemail", case[5], "--disableutedit", case[6], "--expiry", case[7], "--expiryother", case[8], "--reason", case[9], "--autoblock", case[11], "--confirm", case[14]]
            if case[2]:
                params.extend(["--pagerestrictions", case[2]])
            if case[3]:
                params.extend(["--namespacerestrictions", case[3]])
            if case[10]:
                params.extend(["--reasonother", case[10]])
            if case[12]:
                params.extend(["--redirect", case[12]])
            if case[13]:
                params.extend(["--previoustarget", case[13]])
            if case[14]:
                params.extend(["--hideuser", case[14]])

            blk = SpecialBlock("--target", target, *params)
            response = blk.run()

            # Check the database
            # db = ""
            # if response['error'] is None and len(response['messages']) == 0:
            #     rows = mysql_query("SELECT log_params FROM logging WHERE log_type='block' AND log_action='block' AND log_title=%s ORDER BY log_id DESC LIMIT 1", params=[target], dbname="wiki")
            #     for row in rows:
            #         db = str(row)

            # Log
            log = ""
            if response['error'] is None and len(response['messages']) == 0:
                if case[14]:
                    logs = self.site.logevents(logtype="suppress", user=self.site.username(), total=1)
                else:
                    logs = self.site.logevents(logtype="block", user=self.site.username(), total=1)
                for logentry in logs:
                    log = logentry.data

            final_results.append({"params": params,
                                  "messages": response['messages'],
                                  "error": response['error'],
                                  "log": log})
                                  # "db": db})

            Unblock("--user", target).run()

        with open(outputfile, 'a', newline='') as jsonfile:
            try: 
                json.dump(final_results, jsonfile)
            except:
                print(final_results)
                jsonfile.write(final_results)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = SpecialBlockSystematic(*args)
    app.run()


if __name__ == '__main__':
    main()
