#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import pywikibot
import json

from bs4 import BeautifulSoup

from pywikibot.comms.http import request

class SpecialBlock(object):

    """Submit Special:Block"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('--target', required=True)
        parser.add_argument('--editing', type=int, choices=[0, 1], default=1)
        parser.add_argument('--restriction', type=str, choices=["sitewide", "partial"], default="sitewide")
        parser.add_argument('--pagerestrictions', default="")
        parser.add_argument('--namespacerestrictions', default="")
        parser.add_argument('--createaccount', type=int, choices=[0, 1], default=1)
        parser.add_argument('--disableemail', type=int, choices=[0, 1], default=0)
        parser.add_argument('--disableutedit', type=int, choices=[0, 1], default=0)
        parser.add_argument('--disableupload', type=int, choices=[0, 1], default=0)
        parser.add_argument('--expiry', default="other")
        parser.add_argument('--expiryother', default="indefinite")
        parser.add_argument('--reason', default="other")
        parser.add_argument('--reasonother', default="")
        parser.add_argument('--autoblock', type=int, choices=[0, 1], default=1)
        parser.add_argument('--hideuser', type=int, choices=[0, 1], default=0)
        parser.add_argument('--redirect', default="")
        parser.add_argument('--previoustarget', default="")
        parser.add_argument('--confirm', type=int, choices=[0, 1], default=0)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        # Get the/an edit token
        self.site.login()
        response = request(site=self.site, uri="wiki/Special:Block", method='GET').text
        soup = BeautifulSoup(response, features='lxml')
        edit_token = soup.find("input", id="wpEditToken")["value"]

        params = {
            "wpTarget": self.options.target,
            "wpEditingRestriction": self.options.restriction,
            "wpPageRestrictions": self.options.pagerestrictions,
            "wpNamespaceRestrictions": self.options.namespacerestrictions,
            "wpExpiry": self.options.expiry,
            "wpExpiry-other": self.options.expiryother,
            "wpReason": self.options.reason,
            "wpReason-other": self.options.reasonother,
            "title": "Special:Block",
            "redirectparams": self.options.redirect,
            "wpPreviousTarget": self.options.previoustarget,
            "wpEditToken": edit_token
        }
        if self.options.editing:
            params["wpEditing"] = 1
        if self.options.createaccount:
            params['wpCreateAccount'] = 1
        if self.options.disableemail:
            params['wpDisableEmail'] = 1
        if self.options.disableutedit:
            params['wpDisableUTEdit'] = 1
        if self.options.disableupload:
            params['wpActionRestrictions[]'] = 1
        if self.options.autoblock:
            params["wpAutoBlock"] = 1
        if self.options.hideuser:
            params["wpHideUser"] = 1
        if self.options.confirm:
            params["wpConfirm"] = 1

        response = request(site=self.site, uri="wiki/Special:Block", method='POST', data=params).text

        soup = BeautifulSoup(response, features='lxml')

        success = soup.find("div", id="mw-content-text")
        if success:
            success = success.get_text()

        messages = soup.find_all("div", class_="oo-ui-fieldLayout-messages")
        messagesdict = {}
        for message in messages:
            if 'data-mw-modules' in message.parent.attrs:
                # inputname = message.parent.attrs['data-mw-modules']
                parentoouijson = json.loads(message.parent.attrs['data-ooui'])
                inputname = parentoouijson['fieldWidget']['tag']
            else:
                inputname = "top"
            messagesdict[inputname] = message.get_text()

        error = soup.find("div", class_="errorbox")
        if error:
            error = error.get_text()

        return {
            'html': success,
            'messages': messagesdict,
            'error': error
        }


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = SpecialBlock(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
