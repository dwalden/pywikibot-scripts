#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
List pages on the current user's watchlist that were changed within the
given time period, ordered by time of the last change of the watched page.

The following parameters are supported:

TODO

"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import pprint

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class Watchlist(object):

    """Get all pages on the current user's watchlist."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-n', '--namespaces', default="", help="Only list pages in the given namespaces. Separated by |.")
        parser.add_argument('-l', '--limit', default=10, help="How many total results to return per request.")
        parser.add_argument('-a', '--allrev', action='store_true', help="Include multiple revisions of the same page within given timeframe.")
        parser.add_argument('-s', '--start', default="", help="The timestamp to start enumerating from.")
        parser.add_argument('-e', '--end', default="", help="The timestamp to end enumerating.")
        parser.add_argument('-u', '--user', default="", help="Only list changes by this user.")
        parser.add_argument('-x', '--excludeuser', default="", help="Don't list changes by this user.")
        parser.add_argument('-d', '--dir', default="", help="In which direction to enumerate: 'newer' or 'older'.")
        parser.add_argument('-p', '--prop', default="", help="Which additional properties to get: comment, flags, ids, loginfo, notificationtimestamp, parsedcomment, patrol, sizes, tags, timestamp, title, user, userid.")
        parser.add_argument('-w', '--show', default="", help="Show only items that meet these criteria: !anon, !autopatrolled, !bot, !minor, !patrolled, !unread, anon, autopatrolled, bot, minor, patrolled, unread.")
        parser.add_argument('-t', '--type', default="", help="Which types of changes to show: categorize, edit, external, log, new.")
        parser.add_argument('-o', '--owner', default="", help="Used along with wltoken to access a different user's watchlist.")
        parser.add_argument('-k', '--token', default="", help="A security token (available in the user's preferences) to allow access to another user's watchlist.")
        parser.add_argument('-g', '--generator', action='store_true', help="Do a generator?")
        parser.add_argument('-c', '--custom', default="", help="Custom arguments.")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        pp = pprint.PrettyPrinter()
        self.site.login()
        if self.options.generator:
            params = {'action': 'query',
                      'generator': 'watchlist',
                      'format': 'json'}

            if self.options.prop:
                params['prop'] = self.options.prop
            if self.options.custom:
                custom_args = self.options.custom.split("&")
                for custom_arg in custom_args:
                    (arg, val) = custom_arg.split("=")
                    params[arg] = val

        else:
            params = {'action': 'query',
                      'list': 'watchlist',
                      'wllimit': self.options.limit,
                      'format': 'json'}

            if self.options.namespaces:
                params['wlnamespace'] = self.options.namespaces
            if self.options.allrev:
                params['wlallrev'] = self.options.allrev
            if self.options.start:
                params['wlstart'] = self.options.start
            if self.options.end:
                params['wlend'] = self.options.end
            if self.options.user:
                params['wluser'] = self.options.user
            if self.options.excludeuser:
                params['wlexcludeuser'] = self.options.excludeuser
            if self.options.dir:
                params['wldir'] = self.options.dir
            if self.options.prop:
                params['wlprop'] = self.options.prop
            if self.options.show:
                params['wlshow'] = self.options.show
            if self.options.type:
                params['wltype'] = self.options.type

            if self.options.owner:
                params['wlowner'] = self.options.owner
            if self.options.token:
                params['wltoken'] = self.options.token

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            return response
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Watchlist(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
