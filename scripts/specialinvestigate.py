#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Investigate user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import pywikibot
import json
import ipaddress
import random

from bs4 import BeautifulSoup

from pywikibot.comms.http import request
from pywikibot.data.mysql import mysql_query

class SpecialInvestigate(object):

    """Submit Special:Investigate"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        # Get the/an edit token
        response = request(site=self.site, uri="wiki/Special:Investigate", method='GET').text
        soup = BeautifulSoup(response)
        edit_token = soup.find("input", id="wpEditToken")["value"]

        query = """
SELECT actor_name FROM actor ORDER BY RAND() LIMIT 5;
# SELECT actor_name FROM actor WHERE actor_user IS NOT NULL AND actor_name LIKE "% %" ORDER BY RAND() LIMIT 1;
# SELECT actor_name FROM actor WHERE actor_user IS NOT NULL ORDER BY RAND() LIMIT 1;
"""
        actors = mysql_query(query, dbname=self.site.dbName())

        targets = ""
        expected = []
        for actor in actors:
            username = actor['actor_name'].decode('utf-8')
            expected.append(username)

            random_integer = 2 # random.randint(1, 2)
            for i in range(0, random_integer):
                try:
                    ipaddr = ipaddress.ip_address(username)
                    select = random.randint(1, 2)
                    if select == 1:
                        final = ipaddr.exploded
                    else:
                        final = ipaddr.compressed
                except:
                    select = random.randint(1, 2)
                    if select == 1:
                        final = username.replace(" ", "_")
                    else:
                        final = username
                targets += final + "\n"

        expected_normalised = []
        for actor in expected:
            username = actor
            try:
                ipaddr = ipaddress.ip_address(username)
                final = ipaddr.compressed
            except:
                final = username
            expected_normalised.append(final)
        expected_normalised = list(set(expected_normalised))

        # POST the investigation
        params = {
            "targets": targets,
            "duration": "",
            "reason": "foobar",
            "wpEditToken": edit_token,
            "title": "Special:Investigate",
            "redirectparams": ""
        }
        response = request(site=self.site, uri="wiki/Special:Investigate", method='POST', body=params).text

        soup = BeautifulSoup(response)
        results = soup.find("label", id="ooui-php-8")
        actual = []
        for name in results.find_all("strong"):
            actual.append(name.get_text())

        actual_normalised = []
        for name in actual:
            try:
                ipaddr = ipaddress.ip_address(name)
                final = ipaddr.compressed
            except:
                final = name
            actual_normalised.append(final)
        actual_normalised = list(set(actual_normalised))

        return [expected, actual, actual == expected, len(actual) == len(expected), expected_normalised, actual_normalised, expected_normalised == actual_normalised, targets]


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = SpecialInvestigate(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
