#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import csv
from datetime import datetime
import requests
import copy

from bs4 import BeautifulSoup

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError
from pywikibot.data.mysql import mysql_query
from pywikibot.comms.http import request

class SecurePollAcl(object):

    """SecurePollAcl user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        outputfile = "securepoll_acl_{}_{}_{}_{}.csv".format(self.site.code, self.site.family, self.site.username(), datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))

        rows = []
        fields = ["Election Name", "Home Wiki", "Private?", "User", "Is Admin of the Election?", "Is electionadmin?", "Edit", "Translate", "List", "Details", "Strike", "Voter Eligibility", "Dump", "Tally", "Tally (POST)"]

        query = """
SELECT el_entity, el_title, el_auth_type, p1.pr_value AS Admins, p2.pr_value AS Private, p3.pr_value AS Home, GROUP_CONCAT(votes.vote_id) AS Votes FROM securepoll_elections
LEFT JOIN securepoll_properties AS p1 ON p1.pr_entity=el_entity AND p1.pr_key="admins"
LEFT JOIN securepoll_properties AS p2 ON p2.pr_entity=el_entity AND p2.pr_key="voter-privacy"
LEFT JOIN securepoll_properties AS p3 ON p3.pr_entity=el_entity AND p3.pr_key="wikis"
LEFT JOIN securepoll_votes AS votes ON votes.vote_election=el_entity
GROUP BY el_entity;
"""

        elections = mysql_query(query, dbname=self.site.dbName())

        for election in elections:
            if self.site.username() in election['Admins'].decode().split('|'):
                isAdmin = True
            else:
                isAdmin = False
            row = {
                'Election Name': election['el_title'],
                'Home Wiki': election['Home'],
                'Private?': election['Private'],
                'User': self.site.username(),
                'Is Admin of the Election?': isAdmin,
                'Is electionadmin?': self.site.has_group('electionadmin'),
            }

            # /edit
            response = request(site=self.site, uri="wiki/Special:SecurePoll/edit/{}".format(election['el_entity']), method='GET').text
            soup = BeautifulSoup(response)
            resp_text = soup.find("div", id="mw-content-text").get_text()
            row['Edit'] = resp_text.split("\n")[0]

            # /translate
            params = {'trans_67_title': 'jklfjdsfkljksdlfjsdklfjslk',
                      'trans_67_intro': '',
                      'trans_67_jump-text': '',
                      'trans_67_return-text': '',
                      'trans_67_unqualified-error': '',
                      'trans_68_text': 'jklj',
                      'trans_69_text': 'jkljl',
                      'comment': ''}
            response = request(site=self.site, uri="wiki/Special:SecurePoll/translate/{}/en?action=submit".format(election['el_entity']), method='POST', data=params).text
            soup = BeautifulSoup(response)
            resp_text = soup.find("div", id="mw-content-text").get_text()
            row['Translate'] = resp_text.split("\n")[0]

            # /list
            response = request(site=self.site, uri="wiki/Special:SecurePoll/list/{}".format(election['el_entity']), method='GET').text
            soup = BeautifulSoup(response)
            resp_text = soup.find("div", id="mw-content-text").get_text()
            row['List'] = resp_text.split("\n")[0]

            row['Details'] = []
            row['Strike'] = []
            if election['Votes']:
                for vote in election['Votes'].split(','):

                    # /details
                    response = request(site=self.site, uri="wiki/Special:SecurePoll/details/{}".format(vote), method='GET').text
                    soup = BeautifulSoup(response)
                    resp_text = soup.find("div", id="mw-content-text").get_text()
                    resp = resp_text.split("\n")[0]
                    if resp not in row['Details']:
                        row['Details'].append(resp)

                    # strike
                    req = Request(site=self.site, parameters={'action': 'strikevote',
                                                              'format': 'json',
                                                              'option': 'strike',
                                                              'voteid': vote,
                                                              'reason': 'foobar',
                                                              'token': self.site.get_tokens(["csrf"])["csrf"]})
                    try:
                        response = req.submit()
                        resp = response['strikevote']['status']
                    except APIError as err:
                        resp = err.info
                    if resp not in row['Strike']:
                        row['Strike'].append(resp)

            # /votereligibility
            response = request(site=self.site, uri="wiki/Special:SecurePoll/votereligibility/{}".format(election['el_entity']), method='GET').text
            soup = BeautifulSoup(response)
            resp_text = soup.find("div", id="mw-content-text").get_text()
            row['Voter Eligibility'] = resp_text.split("\n")[0]

            # /dump
            response = request(site=self.site, uri="wiki/Special:SecurePoll/dump/{}".format(election['el_entity']), method='GET').text
            soup = BeautifulSoup(response)
            content = soup.find("div", id="mw-content-text")
            if content:
                resp_text = content.get_text()
            else:
                resp_text = response
            row['Dump'] = resp_text.split("\n")[0]

            # /tally
            response = request(site=self.site, uri="wiki/Special:SecurePoll/tally/{}".format(election['el_entity']), method='GET').text
            soup = BeautifulSoup(response)
            resp_text = soup.find("div", id="mw-content-text").get_text()
            row['Tally'] = resp_text.split("\n")[0]

            # /tally (post)
            response = request(site=self.site, uri="wiki/Special:SecurePoll/tally/{}".format(election['el_entity']), method='POST').text
            soup = BeautifulSoup(response)
            resp_text = soup.find("div", id="mw-content-text").get_text()
            row['Tally (POST)'] = resp_text.split("\n")[0]

            rows.append(row)

        with open(outputfile, 'a', newline='') as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=False, extrasaction="ignore")
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = SecurePollAcl(*args)
    app.run()


if __name__ == '__main__':
    main()

