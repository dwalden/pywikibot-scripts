#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
List recent changes.

The following parameters are supported:

TODO
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals

import argparse
import os
import sys
import tempfile

import pywikibot

from pywikibot.data.api import Request, APIError


class WatchlistFeed(object):

    """List Recent Changes"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-f', '--format', default="")
        parser.add_argument('-i', '--hours', default="")
        parser.add_argument('-l', '--linktosections', default="")
        parser.add_argument('-a', '--allrev', action='store_true')
        parser.add_argument('-o', '--owner', default="")
        parser.add_argument('-k', '--token', default="")
        parser.add_argument('-s', '--show', default="")
        parser.add_argument('-t', '--type', default="")
        parser.add_argument('-x', '--excludeuser', default="")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        params = {'action': 'feedwatchlist'}

        if self.options.format:
            params['feedformat'] = self.options.format
        if self.options.hours:
            params['hours'] = self.options.hours
        if self.options.linktosections:
            params['linktosections'] = self.options.linktosections
        if self.options.allrev:
            params['allrev'] = 1
        if self.options.owner:
            params['wlowner'] = self.options.owner
        if self.options.token:
            params['wltoken'] = self.options.token
        if self.options.show:
            params['wlshow'] = self.options.show
        if self.options.type:
            params['wltype'] = self.options.type
        if self.options.excludeuser:
            params['wlexcludeuser'] = self.options.excludeuser

        r = Request(site=self.site, parameters=params)
        try:
            paramstring = r._http_param_string()
            use_get, uri, body, headers = r._get_request_params(True, paramstring)
            rawdata, use_get = r._http_request(use_get, uri, body, headers, paramstring)
            return rawdata
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error.")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = WatchlistFeed(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
