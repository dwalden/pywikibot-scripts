#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Tag a revision, recent change or log entry.

The following parameters are supported:

-p                Properties (as per "rcprop" in https://www.mediawiki.org/wiki/API:RecentChanges)
-n                Number of recent changes to return
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals

import argparse
import os
import sys
import tempfile

import pywikibot

from pywikibot.data.api import Request, APIError


class ListRecentChanges(object):

    """List Recent Changes"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-p', '--properties', default="")
        parser.add_argument('-n', '--num', default=10)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        rc = self.site.recentchanges(total=self.options.num)
        for change in rc:
            if self.options.properties != "":
                pywikibot.output({k: change[k] for k in self.options.properties.split(",")})
            else:
                pywikibot.output(change)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = ListRecentChanges(*args)
    app.run()


if __name__ == '__main__':
    main()
