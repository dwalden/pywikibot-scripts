#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2021 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import argparse
import pywikibot
import csv
import json
import os

from pywikibot.comms import http
from datetime import datetime
from time import time


def dict_factory(cursor, row):
    fields = [column[0] for column in cursor.description]
    return {key: value for key, value in zip(fields, row)}


class AllParamsApi(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-m', '--module', type=str, required=True)
        parser.add_argument('-q', '--query', type=str, required=True)
        parser.add_argument('-d', '--database', type=str, required=True)
        parser.add_argument('-H', '--mysql-host', type=str)
        parser.add_argument('-u', '--username', type=str, default='root')
        parser.add_argument('-p', '--password', type=str, default='main_root_password')
        parser.add_argument('-P', '--port', type=str, default=3306)
        parser.add_argument('-r', '--repeat', type=int, default=1)
        parser.add_argument('-s', '--save', action='store_true')
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)
        self.site.login()

        if self.options.mysql_host:
            import pymysql
            connection = pymysql.connect(host=self.options.host,
                                         port=self.options.port,
                                         user=self.options.username,
                                         password=self.options.password,
                                         database=self.options.database,
                                         cursorclass=pymysql.cursors.DictCursor)
        else:
            import sqlite3
            connection = sqlite3.connect(self.options.database)
            connection.row_factory = dict_factory

        paraminfo_req = self.site.simple_request(action='paraminfo', modules=self.options.module)
        paraminfo_response = paraminfo_req.submit()
        prefix = paraminfo_response['paraminfo']['modules'][0]['prefix']
        group = paraminfo_response['paraminfo']['modules'][0]['group']
        module_name = paraminfo_response['paraminfo']['modules'][0]['name']
        all_params = paraminfo_response['paraminfo']['modules'][0]['parameters']
        required_params = {'format': 'json'}
        rows = []
        fields = []

        for param in all_params:
            if 'tokentype' in param:
                required_params["{}{}".format(prefix, param['name'])] = self.site.get_tokens([param['tokentype']])[param['tokentype']]

        cursor = connection.cursor()
        cursor.execute(self.options.query)
        params_from_db = cursor.fetchall()

        for param_data in params_from_db:

            param_data.update(required_params)
            param_data[group] = module_name
            if group != 'action':
                param_data['action'] = 'query'
            pywikibot.output(param_data)

            for i in range(0, self.options.repeat):
                try:
                    start = time()
                    response = http.session.post("{}://{}{}/api.php".format(self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath()),
                                                 data=param_data, headers={'PARAMS': str(param_data)})
                    end = time()
                    param_data['time'] = end - start
                    param_data['repeat'] = i

                    if 'debuginfo' in response.json():
                        for query in response.json()['debuginfo']['queries']:
                            if query['function'] not in param_data:
                                param_data[query['function']] = query['time']
                            else:
                                param_data[query['function']] += query['time']

                    fields = list(set(fields + list(param_data.keys())))
                    rows.append(param_data)

                    if self.options.save:
                        dir_name = "{}_{}".format(self.options.module, date)
                        if not os.path.isdir(dir_name):
                            os.mkdir(dir_name)
                        with open("{}/{}.json".format(dir_name, '_'.join([str(param) for param in param_data.values() if param not in required_params.values() and type(param) is not float])), 'w') as jsonout:
                            respjson = response.json()
                            # TODO: Don't hardcode this
                            if 'edits' in respjson['query']['checkuser']:
                                respjson['query']['checkuser']['edits'].sort(key=lambda x: str(x))
                            json.dump(respjson, jsonout, indent=1)
                except:
                    param_data['time'] = None
                    rows.append(param_data)
                    pywikibot.output("Exception sending API request")

        with open("all_params_api_db_{}_{}.csv".format(self.site.family.langs[self.site.code], date), "a") as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=None, extrasaction="ignore")
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = AllParamsApi(*args)
    app.run()


if __name__ == '__main__':
    main()
