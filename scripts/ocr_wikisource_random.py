#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
The following parameters are supported:

"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import os
import argparse
import requests
import urllib.parse
import pywikibot

from pywikibot import FilePage

from tabulate import tabulate

class OcrWikisourceRandom(object):

    """OcrWikisourceRandom user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=10)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        # self.site.throttle.setDelays(0, 0)
        # self.site.login()

        pages = self.site.randompages(total=self.options.total, namespaces="Page")

        # results = {"Title": [], 1: [], 2: [], 3: [], 4: [], 5: []}
        for page in pages:
            pywikibot.output(page)
            # Parse the page title and (if applicable) page number
            foo = page.title(with_ns=False).split("/")
            if len(foo) > 1:
                title, pg = foo
            else:
                title = foo[0]
                pg = False
            # results["Title"].append(title)

            # Try to get file from Commons. If it is not on Commons, I assume it
            # is local to the current wiki.
            try:
                commons_site = pywikibot.Site('commons', 'commons')
                image = FilePage(commons_site, title)
                file_url = image.get_file_url()
                code = "commons"
            except:
                image = FilePage(self.site, title)
                file_url = image.get_file_url()
                code = self.site.code

            lang = self.site.lang
            # I make some assumptions about where the jpg image of the original
            # pdf/djvu page is.
            if pg:
                if lang in ["bn"]:
                    translation_array = {"০": "0", "১": "1", "২": "2", "৩": "3", "৪": "4", "৫": "5", "৬": "6", "৭": "7", "৮": "8", "৯": "9"}
                    table = pg.maketrans(translation_array)
                    t_pg = pg.translate(table)
                elif lang in ["hi"]:
                    translation_array = {"०": "0", "१": "1", "२": "2", "३": "3", "४": "4", "५": "5", "६": "6", "७": "7", "८": "8", "९": "9"}
                    table = pg.maketrans(translation_array)
                    t_pg = pg.translate(table)
                elif lang in ["gu"]:
                    translation_array = {"૦": "0", "૧": "1", "૨": "2", "૩": "3", "૪": "4", "૫": "5", "૬": "6", "૭": "7", "૮": "8", "૯": "9"}
                    table = pg.maketrans(translation_array)
                    t_pg = pg.translate(table)
                elif lang in ["fa"]:
                    translation_array = {"۰": "0", "۱": "1", "۲": "2", "۳": "3", "۴": "4", "۵": "5", "۶": "6", "۷": "7", "۸": "8", "۹": "9"}
                    table = pg.maketrans(translation_array)
                    t_pg = pg.translate(table)
                elif lang in ["kn"]:
                    translation_array = {"೦": "0", "೧": "1", "೨": "2", "೩": "3", "೪": "4", "೫": "5", "೬": "6", "೭": "7", "೮": "8", "೯": "9"}
                    table = pg.maketrans(translation_array)
                    t_pg = pg.translate(table)
                elif lang in ["or"]:
                    translation_array = {"୦": "0", "୧": "1", "୨": "2", "୩": "3", "୪": "4", "୫": "5", "୬": "6", "୭": "7", "୮": "8", "୯": "9"}
                    table = pg.maketrans(translation_array)
                    t_pg = pg.translate(table)
                else:
                    t_pg = pg

                url = "{}/page{}-1024px-{}.jpg".format(file_url, t_pg, title)
                url = url.replace("/{}/".format(code), "/{}/thumb/".format(code))
            else:
                url = file_url
            url = urllib.parse.quote(url, safe="")

            for case in ["prod", "test", "google"]:
                if case == "prod":
                    engine = "tesseract"
                    env = "ocr"
                elif case == "test":
                    engine = "tesseract"
                    env = "ocr-test"
                else:
                    engine = "google"
                    env = "ocr-test"
                req = "https://{}.wmcloud.org/api.php?image={}&langs[]={}&engine={}".format(env, url, lang, engine)
                pywikibot.output(req)
                response = requests.get(req, headers={'user-agent': 'Mozilla/5.0 (X11; Linux ppc64le; rv:78.0) Gecko/20100101 Firefox/78.0'})
                if response.status_code == 200:
                    out = response.json()['text']
                    if not os.path.isdir("{}".format(lang)):
                        os.mkdir("{}".format(lang))
                    if pg:
                        if not os.path.isdir("{}/{}_{}".format(lang, title, pg)):
                            os.mkdir("{}/{}_{}".format(lang, title, pg))
                        f = open("{}/{}_{}/{}.txt".format(lang, title, pg, case), "w")
                    else:
                        if not os.path.isdir("{}/{}".format(lang, title)):
                            os.mkdir("{}/{}".format(lang, title))
                        f = open("{}/{}/{}.txt".format(lang, title, case), "w")
                    f.write(out)
                    f.close()
                else:
                    pywikibot.output(req)
                    pywikibot.output(response.text)

        # pywikibot.output(tabulate(results))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = OcrWikisourceRandom(*args)
    response = app.run()
    # pywikibot.output(response)


if __name__ == '__main__':
    main()
