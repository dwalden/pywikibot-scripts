#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Tag a revision, recent change or log entry.

The following parameters are supported:

-p                Page ID
-n                Number of times to simulate clicking "Next"
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class PagesNotInNPPQueue(object):

    """List Recent Changes"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-p', '--pageid', required=True)
        parser.add_argument('-s', '--start', required=True)
        parser.add_argument('-d', '--direction', default='oldestfirst')
        parser.add_argument('-n', '--num', default=10)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        offset = self.options.start
        pageoffset = self.options.pageid
        for i in range(0, int(self.options.num)):
            r = Request(site=self.site, parameters={'action': 'pagetriagelist',
                                                    'namespace': 0,
                                                    'showunreviewed': 1,
                                                    'showothers': 1,
                                                    'showdeleted': 1,
                                                    'showredirs': 1,
                                                    'nppDir': self.options.direction,
                                                    'dir': self.options.direction,
                                                    'limit': 1,
                                                    'mode': 'npp',
                                                    'offset': offset,
                                                    'pageoffset': pageoffset})
            response = r.submit()
            pywikibot.output("Before: {} {}".format(offset, pageoffset))
            offset = response['pagetriagelist']['pages'][0]['creation_date_utc']
            pageoffset = response['pagetriagelist']['pages'][0]['pageid']
            pywikibot.output("After: {} {} {}".format(response['pagetriagelist']['pages'][0]['title'], offset, pageoffset))
            pywikibot.output("Result: {}".format(response['pagetriagelist']['result']))
            pywikibot.output(response['pagetriagelist']['pages_missing_metadata'])


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = PagesNotInNPPQueue(*args)
    app.run()


if __name__ == '__main__':
    main()
