#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
TODO

The following parameters are supported:

TODO
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import string
import random

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class ValidCharacters(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        # self.site.login()
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)

        # characters = string.punctuation
        # characters = "¤äÂÉ÷¤ìÙ¨ÓÜ©Òã±öíÝëßÜ³ßëÛÂ®ÓâÉ¾°È°ß²äèúöòÂ¸ÀþÚ¸®ÿÜ½¯ºÔÈ¦ïòÊ¼íïÝ¦"
        characters = []
        # for i in range(0, 500):
        #     characters.append(chr(random.randint(0, int("10FFFF", 16))))
        for i in range(int("2000", 16), int("206F", 16)):
            print(i)
            characters.append(chr(i))
        valid_characters = []
        invalid_characters = []

        for character in characters:
            username = "Random{}".format(character)
            params = {'action': 'query',
                      'list': 'users',
                      'ususers': username,
                      'usprop': 'cancreate'}

            r = Request(site=self.site, parameters=params)
            try:
                response = r.submit()
                pywikibot.output(response)
                thingy = response['query']['users'][0]
                if 'invalid' in thingy or ('cancreate' in thingy and thingy['cancreate']) or 'cancreateerror' in thingy:
                    invalid_characters.append(character)
                else:
                    valid_characters.append(character)
            except APIError as err:
                pywikibot.output(err)
            except:
                pywikibot.output("Some other error")

        return {'valid': valid_characters,
                'invalid': invalid_characters}


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = ValidCharacters(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
