#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
The following parameters are supported:

"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import requests
import pywikibot


class WikisourceOcr(object):

    """WikisourceOcr user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--image', required=True)
        parser.add_argument('-l', '--lang', default="")
        parser.add_argument('-e', '--engine', default="google")
        parser.add_argument('-u', '--url', default="https://ocr-test.wmcloud.org")
        parser.add_argument('-p', '--psm', type=int, default=3)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        req = "{}/api.php?image={}&lang={}&engine={}&psm={}".format(self.options.url, self.options.image, self.options.lang, self.options.engine, self.options.psm)
        response = requests.get(req)
        return response.json()


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = WikisourceOcr(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
