#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
The following parameters are supported:

"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import requests
import urllib.parse
import pywikibot

from pywikibot import FilePage

class OcrWikisourceRandom(object):

    """OcrWikisourceRandom user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=10)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        # self.site.throttle.setDelays(0, 0)
        # self.site.login()

        pages = self.site.randompages(total=self.options.total, namespaces="Page")

        for page in pages:
            # pywikibot.output(page)
            title, pg = page.title(with_ns=False).split("/")
            commons_site = pywikibot.Site('commons', 'commons')
            image = FilePage(commons_site, title)
            # pywikibot.output(image.get_file_url())
            url = "{}/page{}-2134px-{}.jpg".format(image.get_file_url(), pg, title)
            url = url.replace("/commons/", "/commons/thumb/")
            url = urllib.parse.quote(url, safe="")
            out = {}
            compare = True
            for lang in ["es", "pt"]:
                engine = "google"
                req = "https://ocr-test.wmcloud.org/api.php?image={}&lang={}&engine={}".format(url, lang, engine)
                # req = "http://localhost:8000/?image={}&lang={}&engine={}".format(url, lang, engine)
                # pywikibot.output(req)
                response = requests.get(req)
                if response.status_code == 200:
                    out[lang] = response.json()['text']
                else:
                    pywikibot.output(response.status_code)
                    pywikibot.output(req)
                    compare = False

            if compare:
                res = out['es'] == out['pt']
            else:
                res = "Cannot compare"
            pywikibot.output("{}: {}".format(page, res))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = OcrWikisourceRandom(*args)
    response = app.run()
    # pywikibot.output(response)


if __name__ == '__main__':
    main()
