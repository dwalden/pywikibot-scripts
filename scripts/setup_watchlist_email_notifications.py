#!/usr/bin/python
# -*- coding: utf-8 -*-
"""

"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError

from .watch import Watch
from .options import Options

class Setup(object):

    """ """

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        # users = ["Drw123",
        #          "Drwbbv",
        #          "Drwbbv1",
        #          "Drwbbv1a",
        #          "Drwca",
        #          "Drwpb",
        #          "Drwpb1",
        #          "Drwpb2",
        #          "Drwpb4",
        #          "Drwpb6",
        #          "Drwpr",
        #          "Drwpr1",
        #          "Drwpr2",
        #          "Drwpr3",
        #          "Drwpww",
        #          "Drwpww1"]
        users = ["Drwbbv",
                 "Drwpb",
                 "Drwpbc",
                 "Drwpbc1",
                 "Drwpbc2",
                 "Drwpbc3",
                 "Drwpr",
                 "Drwpr1"]
        # pages = ["User_talk:Drw123",
        #          "User_talk:Drwbbv",
        #          "User_talk:Drwbbv1",
        #          "User_talk:Drwbbv1a",
        #          "User_talk:Drwca",
        #          "User_talk:Drwpb",
        #          "User_talk:Drwpb1",
        #          "User_talk:Drwpb2",
        #          "User_talk:Drwpb4",
        #          "User_talk:Drwpb6",
        #          "User_talk:Drwpr",
        #          "User_talk:Drwpr1",
        #          "User_talk:Drwpr2",
        #          "User_talk:Drwpr3",
        #          "User_talk:Drwpww",
        #          "User_talk:Drwpww1"]
        # pages = ["User_talk:Drwbbv",
        #          "User_talk:Drwpb",
        #          "User_talk:Drwpbc",
        #          "User_talk:Drwpbc1",
        #          "User_talk:Drwpbc2",
        #          "User_talk:Drwpbc3",
        #          "User_talk:Drwpr",
        #          "User_talk:Drwpr1"]
        pages = ["Fffff", "MobileTest"]

        for enotifwatchlistpages in [0, 1]:
            # for enotifusertalkpages in [0, 1]:
            enotifusertalkpages = 1
            for enotifminoredits in [0, 1]:
                # for watched in ["no", "permanently", "temporarily", "expired"]:
                for watched in ["no", "permanently"]:
                    if users:
                        user = users.pop()
                    else:
                        exit()

                    print("| {} | {} | {} | {} | {} |".format(user, enotifwatchlistpages, enotifusertalkpages, enotifminoredits, watched))

                    # opt = Options("-c", "enotifwatchlistpages={}|enotifusertalkpages={}|enotifminoredits={}".format(enotifwatchlistpages, enotifusertalkpages, enotifminoredits), "--user", user)
                    # opt.run()

                    if watched == "permanently":
                        for page in pages:
                            wtch = Watch("-t", page, "--user", user)
                            wtch.run()
                    if watched == "temporarily":
                        for page in pages:
                            wtch = Watch("-t", page, "-e", "1 week", "--user", user)
                            wtch.run()
                    if watched == "expired":
                        for page in pages:
                            wtch = Watch("-t", page, "-e", "1 second", "--user", user)
                            wtch.run()


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Setup(*args)
    app.run()


if __name__ == '__main__':
    main()
