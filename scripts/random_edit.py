#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
RandomBlock user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import ipaddress
import random
import csv

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError

from .edit import Edit

class RandomEdit(object):

    """RandomEdit user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=10)
        # parser.add_argument('-a', '--append', required=True)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.throttle.setDelays(0, 0, True)
        self.site.login()

        pages = self.site.randompages(total=self.options.total, namespaces="0")

        for page in pages:
            edt = Edit("-t", page.title(), "-a", "[[Main_Page]]")
            response = edt.run()
            pywikibot.output(response)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = RandomEdit(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
