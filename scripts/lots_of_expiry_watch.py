#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
RandomBlock user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import random

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError

from .watch import Watch

class ExpiryOptions(object):

    """ExpiryOptions user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=10)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.throttle.setDelays(0, 0)
        self.site.login()

        pages = self.site.randompages(total=self.options.total, namespaces="0")
        expiries = range(1, self.options.total + 1)

        i = 0
        for page in pages:
            wtch = Watch("-t", page.title(), "-e", "{} days".format(expiries[i]))
            response = wtch.run()
            pywikibot.output(response)
            i = i + 1


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = ExpiryOptions(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
