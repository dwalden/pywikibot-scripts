#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import requests

from bs4 import BeautifulSoup

import pywikibot
from pywikibot.comms.http import request

class SecurePollVote(object):

    """SecurePollVote user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--voteid', required=True)
        parser.add_argument('-b', '--blt', required=True)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        question = False
        candidate_ids = False

        with open(self.options.blt) as ballot_file:
            lines = ballot_file.read().splitlines()

            # First line gives us number of candidates and seats
            lines.pop(0)

            # The next lines are the actual votes
            votes = []
            for line in lines:
                # The list of votes end with a "0"
                if line == "0" or line == 0:
                    break

                vote = line.split(" ")

                # First element is number of votes
                num = vote.pop(0)
                # Remove the last element, which are not part of the vote, but
                # part of the syntax of OpenSTV.
                vote.pop()

                if vote and vote[0] != "":
                    for i in range(int(num)):
                        votes.append(vote)

            for vote, preferences in enumerate(votes):

                voter = "Drw{}".format(vote + 4629 + 1)
                self.site = pywikibot.Site(user=voter)
                self.site.login()

                ballot = request(site=self.site, uri="wiki/Special:SecurePoll/vote/{}".format(self.options.voteid), method='GET').text
                soup = BeautifulSoup(ballot, features='lxml')
                form = soup.find("form", class_="oo-ui-layout oo-ui-formLayout")

                if not question or not candidate_ids:
                    select = form.find("select")
                    question = select['name'][:-1]

                    options = select.find_all("option")
                    candidate_ids = {}
                    for option in options:
                        candidate_ids[option.text] = option['value']

                post_data = {}
                for rank, candidate in enumerate(preferences):
                    post_data["{}{}".format(question, rank)] = candidate_ids[candidate]

                post_data['edit_token'] = form.find("input", attrs={'name':"edit_token"})['value']

                response = request(site=self.site, uri="wiki/Special:SecurePoll/vote/{}?action=vote".format(self.options.voteid), method='POST', data=post_data)

                self.site.logout()


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = SecurePollVote(*args)
    app.run()


if __name__ == '__main__':
    main()

