#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Watch or unwatch a page.

The following parameters are supported:

-t                List of Titles, separated by |.
-p                List of Page IDs, separated by |.
-r                List of Revision IDs, separated by |.
-u                Unwatch targets.
-e                Expiry time. May be relative (e.g. 5 months or 2 weeks) or absolute (e.g. 2014-09-18T12:34:56Z). If set to infinite, indefinite, or never, the watch will never expire.
-d                Automatically resolve redirects.
-c                Convert titles to other variants if necessary and supported.
-g                "Get the list of pages to work on by executing the specified query module."
--user            User to login as.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class Watch(object):

    """Watch or unwatch a page."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        if self.options.user:
            self.site = pywikibot.Site(user=self.options.user)
        else:
            self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--titles', default="", help="List of Titles, separated by |.")
        parser.add_argument('-p', '--pages', default="", help="List of Page IDs, separated by |.")
        parser.add_argument('-r', '--revisions', default="", help="List of Revision IDs, separated by |.")
        parser.add_argument('-e', '--expiry', default="", help="Expiry time. May be relative (e.g. 5 months or 2 weeks) or absolute (e.g. 2014-09-18T12:34:56Z), or infinite, indefinite, or never.")
        parser.add_argument('-u', '--unwatch', action='store_true', help="Unwatch targets.")
        parser.add_argument('-d', '--redirects', action='store_true', help="Automatically resolve redirects.")
        parser.add_argument('-c', '--convert', action='store_true', help="Convert titles to other variants if necessary and supported.")
        parser.add_argument('-g', '--generator', default="", help="\"Get the list of pages to work on by executing the specified query module.\"")
        parser.add_argument('-m', '--custom', default="", help="Custom arguments. Specified like: 'arg1=val1&arg2=val2|val3'")
        parser.add_argument('-n', '--nodelay', action='store_true')
        parser.add_argument('--user', default=None, help="User to login as.")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0)
        params = {'action': 'watch',
                  'format': 'json',
                  'token': self.site.get_tokens(["watch"])["watch"]}

        if self.options.titles:
            params['titles'] = self.options.titles
        if self.options.pages:
            params['pageids'] = self.options.pages
        if self.options.revisions:
            params['revids'] = self.options.revisions

        if self.options.expiry:
            params['expiry'] = self.options.expiry

        if self.options.unwatch:
            params['unwatch'] = True
        if self.options.redirects:
            params['redirects'] = True
        if self.options.convert:
            params['converttitles'] = True

        if self.options.generator:
            params['generator'] = self.options.generator
        if self.options.custom:
            for option in self.options.custom.split("&"):
                (arg, val) = option.split("=")
                params[arg] = val

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            return response
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Watch(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
