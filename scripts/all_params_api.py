#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2021 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import argparse
import pywikibot
import csv
import json
import os

from pywikibot.comms import http
from itertools import combinations
from datetime import datetime
from time import time
from copy import copy


class AllParamsApi(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-m', '--module', type=str, required=True)
        parser.add_argument('-s', '--size', type=int, default=2)
        parser.add_argument('-r', '--repeat', type=int, default=1)
        parser.add_argument('-d', '--duplicate', action='store_true')
        parser.add_argument('-c', '--comparison', action='store_true')
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    # {'string', 'limit', 'namespace', 'title', 'expiry', 'integer', 'upload', 'password', 'timestamp', 'boolean', 'text', 'enum', 'user'}
    def random_data(self, param_type, name):
        if name == 'target':
            # return 'Admin'
            # return '172.18.0.1'
            return 'Drw2'
        elif name == 'timecond':
            return '1 year ago'
        elif param_type == "ip" or name == 'xff':
            return '5.5.5.5'
        elif param_type == "limit":
            return 5000
        elif param_type == "integer" or param_type == "namespace" or param_type[-2:] == "id":
            return 1
        elif param_type == "boolean":
            return 1
        elif param_type == "timestamp":
            return "2024-06-06T12:12:12.000Z"
        elif param_type == "range" or param_type == "cidr":
            return "1.2.3.4/28"
        else:
            return param_type

    def run(self):
        """Run the bot."""
        date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)
        self.site.login()

        paraminfo_req = self.site.simple_request(action='paraminfo', modules=self.options.module)
        paraminfo_response = paraminfo_req.submit()
        prefix = paraminfo_response['paraminfo']['modules'][0]['prefix']
        group = paraminfo_response['paraminfo']['modules'][0]['group']
        module_name = paraminfo_response['paraminfo']['modules'][0]['name']
        all_params = paraminfo_response['paraminfo']['modules'][0]['parameters']
        required_params = {'format': 'json'}
        possible_params = []

        for param in all_params:
            if self.options.duplicate and 'multi' in param and type(param['type']) is not list:
                repeat = self.options.size
            else:
                repeat = 1
            for i in range(0, repeat):
                if 'tokentype' in param:
                    required_params["{}{}".format(prefix, param['name'])] = self.site.get_tokens([param['tokentype']])[param['tokentype']]
                elif 'subtypes' in param:
                    for subtype in param['subtypes']:
                        possible_params.append({'name': param['name'],
                                                'value': self.random_data(subtype, param['name'])})
                elif type(param['type']) is list:
                    for dtype in param['type']:
                        possible_params.append({'name': param['name'],
                                                'value': dtype})
                else:
                    possible_params.append({'name': param['name'],
                                            'value': self.random_data(param['type'], param['name'])})

        rows = []
        fields = []
        hopefuls = combinations(possible_params, self.options.size)
        for hopeful in hopefuls:
            for i in range(0, self.options.repeat):
                param_data = copy(required_params)
                param_data[group] = module_name
                if group != 'action':
                    param_data['action'] = 'query'
                for param in hopeful:
                    param_name = "{}{}".format(prefix, param['name'])
                    if param_name in param_data:
                        param_data[param_name] = "{}|{}".format(param_data[param_name], param['value'])
                    else:
                        param_data[param_name] = param['value']
                pywikibot.output(param_data)

                try:
                    start = time()
                    response = http.session.post("{}://{}{}/api.php".format(self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath()),
                                                 data=param_data, headers={'PARAMS': str(param_data)})
                    end = time()
                    param_data['time'] = end - start
                    param_data['repeat'] = i
                    fields = list(set(fields + list(param_data.keys())))
                    rows.append(param_data)
                    pywikibot.output(response)
                    if self.options.comparison:
                        dir_name = "{}_{}".format(self.options.module, date)
                        if not os.path.isdir(dir_name):
                            os.mkdir(dir_name)
                        with open("{}/{}.json".format(dir_name, '_'.join([str(param) for param in param_data.values() if param not in required_params.values() and type(param) is not float])), 'w') as jsonout:
                            json.dump(response.json(), jsonout)
                except:
                    param_data['time'] = None
                    rows.append(param_data)
                    pywikibot.output("Exception sending API request")

        with open("all_params_api_{}_{}.csv".format(self.site.family.langs[self.site.code], date), "a") as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=None, extrasaction="ignore")
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = AllParamsApi(*args)
    app.run()


if __name__ == '__main__':
    main()
