#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
TODO

The following parameters are supported:

TODO
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class CreateAccount(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-u', '--username', required=True)
        parser.add_argument('-p', '--password', required=True)
        parser.add_argument('-e', '--email', default="")
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)
        params = {'action': 'createaccount',
                  'username': self.options.username,
                  'password': self.options.password,
                  'retype': self.options.password,
                  'createreturnurl': "http://foo.com",
                  'format': 'json',
                  'createtoken': self.site.tokens['createaccount']}

        if self.options.email:
            params['email'] = self.options.email

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            pywikibot.output(response)
            self.site.logout()
            return response
        except APIError as err:
            pywikibot.output(err)
            self.site.logout()
        except:
            pywikibot.output("Some other error")
            self.site.logout()


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = CreateAccount(*args)
    app.run()


if __name__ == '__main__':
    main()
