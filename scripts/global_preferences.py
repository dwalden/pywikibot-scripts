#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Tag a revision, recent change or log entry.

The following parameters are supported:

-p                Properties (as per "rcprop" in https://www.mediawiki.org/wiki/API:RecentChanges)
-n                Number of recent changes to return
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError
from pywikibot.exceptions import Error


class GlobalPreferences(object):

    """List Recent Changes"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-l', '--local-override', action='count')
        parser.add_argument('-r', '--reset', default=False)
        parser.add_argument('-c', '--change', default=False)
        parser.add_argument('-o', '--option', default=False)
        parser.add_argument('-v', '--value', default=False)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        if self.options.local_override:
            action = 'globalpreferenceoverrides'
        else:
            action = 'globalpreferences'

        if self.options.change:
            params = {'action': action,
                      'change': self.options.change,
                      'token': self.site.get_tokens(["csrf"])["csrf"]}
        elif self.options.option and self.options.value:
            params = {'action': action,
                      'optionname': self.options.option,
                      'optionvalue': self.options.value,
                      'token': self.site.get_tokens(["csrf"])["csrf"]}
        else:
            exit()

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            return response
        except APIError as err:
            return err
        except Error as err:
            return err
        except:
            return "Some other error"


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Options(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
