#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class Block(object):

    """Block user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('--target', required=True)
        parser.add_argument('--expiry', default="infinite")
        parser.add_argument('--reason', default="")
        parser.add_argument('--partial', action='store_true')
        parser.add_argument('--anononly', action='store_true')
        parser.add_argument('--nocreate', action='store_true')
        parser.add_argument('--autoblock', action='store_true')
        parser.add_argument('--noemail', action='store_true')
        parser.add_argument('--hidename', action='store_true')
        parser.add_argument('--allowusertalk', action='store_true')
        parser.add_argument('--watchuser', action='store_true')
        parser.add_argument('--reblock', action='store_true')
        parser.add_argument('--tags', default="")
        parser.add_argument('--pagerestrictions', default="")
        parser.add_argument('--namespacerestrictions', default="")
        parser.add_argument('--actionrestrictions', default="")
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)
        self.site.login()
        params = {'action': 'block',
                  'user': self.options.target,
                  'expiry': self.options.expiry,
                  'reason': self.options.reason,
                  'format': 'json',
                  "token": self.site.get_tokens(["csrf"])["csrf"]}

        if self.options.partial:
            params['partial'] = True
        if self.options.pagerestrictions != "":
            params['pagerestrictions'] = self.options.pagerestrictions
        if self.options.namespacerestrictions != "":
            params['namespacerestrictions'] = self.options.namespacerestrictions
        if self.options.actionrestrictions != "":
            params['actionrestrictions'] = self.options.actionrestrictions
        if self.options.anononly:
            params['anononly'] = True
        if self.options.nocreate:
            params['nocreate'] = True
        if self.options.autoblock:
            params['autoblock'] = True
        if self.options.noemail:
            params['noemail'] = True
        if self.options.hidename:
            params['hidename'] = True
        if self.options.allowusertalk:
            params['allowusertalk'] = True
        if self.options.watchuser:
            params['watchuser'] = True
        if self.options.reblock:
            params['reblock'] = True
        if self.options.tags != "":
            params['tags'] = self.options.tags

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            return response
        except APIError as err:
            # pywikibot.output(err)
            return err
        except:
            # pywikibot.output("Some other error")
            return "Some other error"


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Block(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
