#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.


# 1. Follow the install instructions for pywikibot:
# https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Copy this file (generate_combination_visibility.py) into the pywikibot "scripts" directory

# Change the visibility of revisions 1 to 15:
# python3 pwb.py generate_combination_visibility -t revision -i {1..15} -lang:en -family:dockerwiki

# Change the visibility of log items 1 to 15:
# python3 pwb.py generate_combination_visibility -t logging -i {1..15} -lang:en -family:dockerwiki


import argparse
import pywikibot

from itertools import combinations, product


class GenerateCombinationVisibility(object):

    """"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--ids', nargs='*', required=True)
        parser.add_argument('-t', '--type', choices=['archive', 'filearchive', 'logging', 'oldimage', 'revision'], required=True)
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)

        combs = []
        for k in [1, 2, 3]:
            combs.extend(list(product(["yes", "no"], combinations(["comment", "content", "user"], k))))

        for i, rev in enumerate(self.options.ids):
            if i < len(combs):
                csrf = self.site.get_tokens(["csrf"]).get("csrf")
                request = self.site.simple_request(action='revisiondelete', type=self.options.type, ids=rev,
                                                   hide="|".join(combs[i][1]), suppress=combs[i][0], token=csrf,
                                                   show="|".join({"comment", "content", "user"} - set(combs[i][1])))
                request.submit()

        pywikibot.output("Number of {}s: {}".format(self.options.type, len(self.options.ids)))
        pywikibot.output("Number of combinations: {}".format(len(combs)))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = GenerateCombinationVisibility(*args)
    app.run()


if __name__ == '__main__':
    main()
