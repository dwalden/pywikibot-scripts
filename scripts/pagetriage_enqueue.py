#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Tag a revision, recent change or log entry.

The following parameters are supported:

-p                Properties (as per "rcprop" in https://www.mediawiki.org/wiki/API:RecentChanges)
-n                Number of recent changes to return
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class PageTriageEnqueue(object):

    """List Recent Changes"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-p', '--pageid', required=True)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        r = Request(site=self.site, parameters={'action': 'pagetriageaction',
                                                'enqueue': 1,
                                                'format': 'json',
                                                'pageid': self.options.pageid,
                                                "token": self.site.get_tokens(["csrf"])["csrf"]})
        try:
            response = r.submit()
            pywikibot.output(response)
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = PageTriageEnqueue(*args)
    app.run()


if __name__ == '__main__':
    main()
