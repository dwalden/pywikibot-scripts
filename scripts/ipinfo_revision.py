#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import csv
from datetime import datetime
import requests
import ipaddress

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError
from pywikibot.comms import http

class IPInfoRevision(object):

    """IPInfoRevision user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', default=10)
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0)

        outputfile = "ipinfo_revision_all_{}_{}_{}_{}.csv".format(self.site.code, self.site.family, self.site.user(), datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))

        rows = []
        fields = []

        revisions = self.site._generator(api.ListGenerator, type_arg='allrevisions', total=self.options.total)

        for page in revisions:
            for revision in page['revisions']:
                response = http.session.get("http://dev.wiki.local.wmftest.net:8080/w/rest.php/ipinfo/v0/revision/{}".format(revision['revid'])).json()

                row = revision
                fields.extend(key for key in list(row.keys()) if key not in fields)

                performer = row['user']
                try:
                    ip_performer_expected = ipaddress.ip_address(performer)
                except:
                    ip_performer_expected = False
                    row['performer match'] = "N/A"

                if 'messageTranslations' in response:
                    row['response'] = response['messageTranslations']['en']
                elif 'info' in response:
                    row['response'] = []
                    for ipinfo in response['info']:
                        row['response'].append(ipaddress.ip_address(ipinfo['subject']))
                    if ip_performer_expected:
                        row['performer match'] = ip_performer_expected in row['response']

                rows.append(row)

        fields.extend(['response', 'performer match'])

        with open(outputfile, 'a', newline='') as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=False)
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = IPInfoRevision(*args)
    app.run()


if __name__ == '__main__':
    main()

