#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import csv
from datetime import datetime
import requests
import copy
import random

from bs4 import BeautifulSoup

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError
from pywikibot.data.mysql import mysql_query
from pywikibot.comms.http import request

class IPInfoContribs(object):

    """IPInfoContribs user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        outputfile = "ipinfo_contribs_{}_{}_{}.csv".format(self.site.code, self.site.family, datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))

        query = """
SELECT ip FROM (SELECT actor_name AS ip, IS_IPV4(actor_name) AS ipv4, IS_IPV6(actor_name) AS ipv6 FROM actor WHERE actor_user IS NULL) AS foo WHERE ipv4=1 OR ipv6=1
"""
        ips = mysql_query(query, dbname=self.site.dbName())

        results = []
        for ip in ips:
            ballot = request(site=self.site, uri="wiki/Special:Contributions/{}".format(ip['ip']), method='GET').text
            soup = BeautifulSoup(ballot)
            widget = soup.find("div", class_="ext-ipinfo-widget")
            if widget:
                results.append(["n/a",
                                ip['ip'],
                                widget.find("div", attrs={"data-property": "location"}).dd.get_text(),
                                widget.find("div", attrs={"data-property": "asn"}).dd.get_text(),
                                widget.find("div", class_="ext-ipinfo-widget-property-source").get_text(),
                                "",
                                widget.find("div", attrs={"data-property": "organization"}).dd.get_text(),
                                widget.find("div", attrs={"data-property": "isp"}).dd.get_text()])
            else:
                pywikibot.output(ip['ip'])

        with open(outputfile, 'w', newline='') as listfile:
            listfile.write(results)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = IPInfoContribs(*args)
    app.run()


if __name__ == '__main__':
    main()

