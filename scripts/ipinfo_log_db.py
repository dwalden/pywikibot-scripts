#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import csv
from datetime import datetime
import requests
import copy

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError
from pywikibot.data.mysql import mysql_query
from pywikibot.comms import http

class IPInfoLogDB(object):

    """IPInfoLogDB user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=1)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        outputfile = "ipinfo_log_{}_{}_{}_{}.csv".format(self.site.code, self.site.family, self.site.user(), datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))

        rows = []
        fields = ["log_id", "Target Hidden", "Performer Hidden", "Suppressed", "Performer", "Performer Is IP", "Performer Suppressed", "Target", "Target Is IP", "Target Suppressed", "Action", "Response"]

#         query = """
# SELECT log_id,
#        IF(1 & log_deleted, "Yes", "No") AS "Target Hidden",
#        IF(4 & log_deleted, "Yes", "No") AS "Performer Hidden",
#        IF(8 & log_deleted, "Yes", "No") AS "Suppressed",
#        actor_name AS Performer,
#        IF(actor_name REGEXP "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|^[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+$", "Yes", "No") AS "Performer Is IP",
#        IF(performer_block.ipb_deleted, "Yes", "No") AS "Performer Suppressed",
#        log_title AS Target,
#        IF(log_title REGEXP "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|^[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+$", "Yes", "No") AS "Target Is IP",
#        IF(target_block.ipb_deleted, "Yes", "No") AS "Target Suppressed",
#        IF(log_type = "suppress", "suppress", "regular") AS Action
# FROM logging
# INNER JOIN actor ON logging.log_actor = actor.actor_id
# LEFT JOIN ipblocks AS performer_block ON actor.actor_user = performer_block.ipb_user
# LEFT JOIN ipblocks AS target_block ON logging.log_title = target_block.ipb_address
# WHERE log_namespace IN (2, 3)
# GROUP BY `Action`, `Performer Suppressed`, `Performer Is IP`, `Target Suppressed`, `Target Is IP`, `Performer Hidden`, `Target Hidden`, `Suppressed`;
# """
        query = """
WITH random_logs AS
(SELECT foo.*, row_number() over(partition by `Action`, `Performer Suppressed`, `Performer Is IP`, `Target Suppressed`, `Target Is IP`, `Performer Hidden`, `Target Hidden`, `Suppressed` order by rand()) as random_sort FROM
(SELECT log_id,
       IF(1 & log_deleted, "Yes", "No") AS "Target Hidden",
       IF(4 & log_deleted, "Yes", "No") AS "Performer Hidden",
       IF(8 & log_deleted, "Yes", "No") AS "Suppressed",
       actor_name AS Performer,
       IF(actor_name REGEXP "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|^[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+$", "Yes", "No") AS "Performer Is IP",
       IF(performer_block.ipb_deleted, "Yes", "No") AS "Performer Suppressed",
       log_title AS Target,
       IF(log_title REGEXP "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|^[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+$", "Yes", "No") AS "Target Is IP",
       IF(target_block.ipb_deleted, "Yes", "No") AS "Target Suppressed",
       # IF(log_type = "suppress", "suppress", "regular") AS Action
       log_type AS Action
FROM logging
INNER JOIN actor ON logging.log_actor = actor.actor_id
LEFT JOIN ipblocks AS performer_block ON actor.actor_user = performer_block.ipb_user
LEFT JOIN ipblocks AS target_block ON logging.log_title = target_block.ipb_address
WHERE log_namespace IN (2, 3)) AS foo)
SELECT * FROM random_logs WHERE random_sort <= {};
""".format(self.options.total)

        logs = mysql_query(query, dbname=self.site.dbName())

        for logentry in logs:
            response = http.session.get("http://{}.wiki.local.wmftest.net:8080/w/rest.php/ipinfo/v0/log/{}".format(self.site.code, logentry['log_id'])).json()

            row = logentry

            if 'messageTranslations' in response:
                row['Response'] = response['messageTranslations']['en']
            elif 'info' in response:
                row['Response'] = []
                for ipinfo in response['info']:
                    row['Response'].append(ipinfo['subject'])

            rows.append(row)

        with open(outputfile, 'a', newline='') as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=False, extrasaction="ignore")
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = IPInfoLogDB(*args)
    app.run()


if __name__ == '__main__':
    main()

