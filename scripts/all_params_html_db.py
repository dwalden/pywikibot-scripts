#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2021 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import argparse
import pywikibot
import csv
import json
import re
import os
import pymysql

from pywikibot.comms import http
from datetime import datetime
from bs4 import BeautifulSoup
from copy import copy


class AllParamsApi(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-u', '--url', type=str, required=True)
        parser.add_argument('-H', '--host', type=str, required=True)
        parser.add_argument('-d', '--database', type=str, required=True)
        parser.add_argument('-q', '--query', type=str, required=True)
        parser.add_argument('-U', '--username', type=str, default='root')
        parser.add_argument('-p', '--password', type=str, default='main_root_password')
        parser.add_argument('-P', '--port', type=str, default=3306)
        parser.add_argument('-r', '--repeat', type=int, default=1)
        parser.add_argument('-Q', '--queries', action='store_true')
        parser.add_argument('-s', '--save', action='store_true')
        parser.add_argument('-i', '--id', type=str, default=False)
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)
        self.site.login()

        connection = pymysql.connect(host=self.options.host,
                                     port=self.options.port,
                                     user=self.options.username,
                                     password=self.options.password,
                                     database=self.options.database,
                                     cursorclass=pymysql.cursors.DictCursor)

        url = "{}://{}{}/index.php?title={}".format(self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), self.options.url)
        get_params = http.session.get(url)
        soup = BeautifulSoup(get_params.text, 'html.parser')
        body = soup.find('div', class_='mw-body-content')
        form = body.find('form')
        method = form['method']
        required_params = {}

        all_inputs = form.find_all('input')
        for param in all_inputs:
            if param['type'] == 'hidden':
                required_params[param['name']] = param['value']

        cursor = connection.cursor()
        cursor.execute(self.options.query)
        params_from_db = cursor.fetchall()
        rows = []
        fields = []

        for param_data in params_from_db:

            param_data.update(required_params)
            pywikibot.output(param_data)

            for i in range(0, self.options.repeat):
                param_data['repeat'] = i

                if method == 'get':
                    response = http.session.get(url, params=param_data)
                else:
                    response = http.session.post(url, data=param_data)
                pywikibot.output(response)

                if self.options.queries:
                    scripts = soup.find_all('script')
                    last_script = str(scripts[-1])
                    json_string = last_script.replace('<script>(RLQ=window.RLQ||[]).push(function(){mw.config.set(', '')
                    json_string = re.sub(r'\);.*</script>', '', json_string)
                    debug = json.loads(json_string)
                    for query in debug['debugInfo']['queries']:
                        foo = copy(param_data)
                        foo['query'] = query['sql']
                        foo['time'] = query['time']
                        foo['call'] = query['function']
                        rows.append(foo)
                    fields = list(set(fields + list(param_data.keys())))

                if self.options.save:
                    dir_name = "{}_{}".format(self.options.url, date)
                    if not os.path.isdir(dir_name):
                        os.mkdir(dir_name)

                    soup = BeautifulSoup(response.text, 'html.parser')
                    if self.options.id:
                        content = soup.find('div', id=self.options.id)
                    else:
                        content = soup.find('div', class_='mw-body-content')

                    with open("{}/{}.html".format(dir_name, '_'.join([str(param) for param in param_data.values() if param not in required_params.values()])), 'w') as htmlout:
                        htmlout.write(str(content))

        if self.options.queries:
            with open("all_params_html_db_{}_{}.csv".format(self.site.family.langs[self.site.code], date), "a") as csvfile:
                dictcsv = csv.DictWriter(csvfile, fields, restval=None, extrasaction="ignore")
                dictcsv.writeheader()
                dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = AllParamsApi(*args)
    app.run()


if __name__ == '__main__':
    main()
