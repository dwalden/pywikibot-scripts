#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Unblock user, IP or IP range.

The following parameters are supported:

-t                Target of the unblock. Must be username, IP or IP range.
-e                Expiry of unblock.
-r                Reason string for unblock.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class Unblock(object):

    """Unblock user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('--id', default=False)
        parser.add_argument('--user', default=False)
        parser.add_argument('--userid', default=False)
        parser.add_argument('--reason', default="")
        parser.add_argument('--tags', default="")
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)
        params = {'action': 'unblock',
                  'reason': self.options.reason,
                  'format': 'json',
                  "token": self.site.get_tokens(["csrf"])["csrf"]}

        if self.options.id:
            params['id'] = self.options.id
        if self.options.user:
            params['user'] = self.options.user
        if self.options.userid:
            params['userid'] = self.options.userid
        if self.options.tags != "":
            params['tags'] = self.options.tags

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            return response
        except APIError as err:
            return err
        except:
            return "Some other error"


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Unblock(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
