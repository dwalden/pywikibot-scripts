#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import csv
from datetime import datetime
import requests
import random

from bs4 import BeautifulSoup

import pywikibot
from pywikibot.comms.http import request

class SecurePollVote(object):

    """SecurePollVote user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        if self.options.user:
            self.site = pywikibot.Site(user=self.options.user)
        else:
            self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--voteid', required=True)
        parser.add_argument('--user', default=None, help="User to login as.")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        outputfile = "securepoll_vote_{}_{}.csv".format(self.site.code, self.site.family)

        rows = []
        fields = ["election", "question", "option", "label", "value", "user"]

        ballot = request(site=self.site, uri="wiki/Special:SecurePoll/vote/{}".format(self.options.voteid), method='GET').text
        soup = BeautifulSoup(ballot, features='lxml')
        form = soup.find("form", class_="oo-ui-layout oo-ui-formLayout")
        inputs = form.find_all("input")
        post_data = {}
        question_choices = {}
        for input_data in inputs:
            if "securepoll" in input_data['name']:
                if input_data['name'] in question_choices.keys():
                    question_choices[input_data['name']]['choices'].append(input_data['value'])
                    optlabel = input_data.parent.parent.parent.find("td", class_="securepoll-ballot-optlabel")
                    if optlabel:
                        question_choices[input_data['name']]['label'].append(optlabel.get_text())
                    else:
                        question_choices[input_data['name']]['label'].append(form.select("label[for={}]".format(input_data['id']))[0].get_text())
                else:
                    question_choices[input_data['name']] = {}
                    question_choices[input_data['name']]['type'] = "schulze" if input_data['type'] == 'number' else input_data['type']
                    optlabel = input_data.parent.parent.parent.find("td", class_="securepoll-ballot-optlabel")
                    if optlabel:
                        question_choices[input_data['name']]['label'] = [optlabel.get_text()]
                    else:
                        question_choices[input_data['name']]['label'] = [form.select("label[for={}]".format(input_data['id']))[0].get_text()]
                    question_choices[input_data['name']]['choices'] = [input_data['value']]
            else:
                post_data[input_data['name']] = input_data['value']

        comments = form.find_all("textarea")
        for comment in comments:
            post_data[comment['name']] = "{}: {}".format(self.site.username(), comment['name'])

        pref = {}
        for key, options in question_choices.items():
            question = key.split("_")[1][1:]

            # Is this a Schulze poll?
            if options['type'] == "schulze":
                if question not in pref.keys():
                    pref[question] = list(range(1, len([x for x in inputs if x['name'].startswith("securepoll_q{}".format(question))]) + 1))
                value = random.choice(pref[question])
                pref[question].remove(value)
                label = options['label'][0]
            else:
                i = random.randint(0, len(options['choices']) - 1)
                value = options['choices'][i]
                label = options['label'][i]

            if len(key.split("_")) > 2:
                option = key.split("_")[2][3:]
            elif len(key.split("_")) <= 2 and options['type'] == "radio":
                option = value
            else:
                option = None

            hmm = True
            if options['type'] == "checkbox":
                hmm = random.choice([True, False])

            if hmm:
                post_data[key] = value
                rows.append({'election':self.options.voteid,
                             'question': question,
                             'option': option,
                             'label': label,
                             'value': value,
                             'user': self.site.username()})

        response = request(site=self.site, uri="wiki/Special:SecurePoll/vote/{}?action=vote".format(self.options.voteid), method='POST', data=post_data)

        if response.status_code == 200:
            with open(outputfile, 'a', newline='') as csvfile:
                dictcsv = csv.DictWriter(csvfile, fields, restval=False, extrasaction="ignore")
                dictcsv.writerows(rows)
        else:
            pywikibot.output(rows)

        self.site.logout()


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = SecurePollVote(*args)
    app.run()


if __name__ == '__main__':
    main()

