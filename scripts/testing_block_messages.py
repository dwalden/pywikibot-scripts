#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import pymysql
import re

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError

from block import Block
from unblock import Unblock
from create_account import CreateAccount


class TestBlockMessage(object):

    """Block user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        self.args = args
        my_args = pywikibot.handle_args(args)
        # parser = argparse.ArgumentParser()
        # self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        # Decide type of block and block reason message
        with open("./parserTests.txt", "r") as wikitext:
            block_reasons = re.findall("!!\s?w?i?k?i?text\n(.*)\n!!", wikitext.read())

        # Login as admin
        self.site.login()
        blockee = "Eve"

        for reason in block_reasons:
            pywikibot.output("Reason: {}".format(reason))
            # Block $user
            block = Block("--target", blockee, "--autoblock", "--nocreate", "--reason", reason, *self.args)
            block_resp = block.run()
            if 'block' not in block_resp:
                continue
            block_id = block_resp['block']['id']
            # Is this a good assumption?
            autoblock_id = int(block_id) + 1
            # Check database
            conn = pymysql.connect("192.168.121.65", "vagrant", "vagrant", "wiki", 3306, charset='utf8mb4')
            cursor = conn.cursor()
            cursor.execute("SELECT comment_text LIKE \"%{}%\" FROM ipblocks INNER JOIN comment ON ipb_reason_id=comment_id ORDER BY ipb_id DESC LIMIT 2".format(reason.replace('"', '""')))
            answers = [row[0] for row in cursor.fetchall()]
            pywikibot.output("Database check ok: {}".format(all(answers)))
            conn.close()
            # Check api.php?action=query&list=blocks
            api_block_list = Request(site=self.site, parameters={'action': 'query',
                                                                 'list': 'blocks',
                                                                 'bkids': "{}|{}".format(block_id, autoblock_id)})
            try:
                response = api_block_list.submit()
                block = response['query']['blocks'][1]
                autoblock = response['query']['blocks'][0]
                autoblock_reason_expected = "Autoblocked because your IP address has been recently used by \"[[User:Eve|Eve]]\".\nThe reason given for Eve's block is \"{}\"".format(block['reason'])
                match = autoblock_reason_expected == autoblock['reason']
                pywikibot.output("API block list check ok: {}".format(match))
            except APIError as err:
                pywikibot.output(err)
            except:
                pywikibot.output("Some other error")

            # Anonymously and as $user
            #  Check error message (e.g. createaccount)
            # ca = CreateAccount("-u", "Drw1234", "-p", "drw1234password", *self.args)
            # ca_response = ca.run()
            # if 'createaccount' in ca_response:
            #     autoblock_reason = ca_response['createaccount']['message']
            # Rinse and repeat
            unblock = Unblock("--user", blockee, *self.args)
            unblock_resp = unblock.run()


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = TestBlockMessage(*args)
    app.run()


if __name__ == '__main__':
    main()
