#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
List recent changes.

The following parameters are supported:

TODO
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals

import argparse
import os
import sys
import tempfile

from bs4 import BeautifulSoup

import pywikibot

from pywikibot.comms.http import request

from .watchlistraw import WatchListRaw

params = {
    "damaging":"likelygood",
    "goodfaith":"likelygood",
    "userExpLevel":"registered",
    "hidemyself":"1",
    "hidebots":"1",
    "hideminor":"1",
    "hidepageedits":"1",
    "hidecategorization":"1",
    "hideWikibase":"1",
    "hidelog":"1",
}


class WatchlistFeed(object):

    """List Recent Changes"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def filter_function(self, x):
        title = x.find("span", class_="mw-changeslist-line-inner")
        if title:
            return title['data-target-page'] in self.watched_pages
        else:
            return False

    def run(self):
        """Run the bot."""
        self.site.login()

        for key, value in params.items():
            filters = {
                'days': 30,
                'urlversion': 2
            }
            filters[key] = value

            wlr = WatchListRaw("-l", "500")
            watched_pages = wlr.run()
            self.watched_pages = list(map(lambda x: x['title'], watched_pages['watchlistraw']))

            filters['limit'] = 500
            rc_resp = request(site=self.site, uri="wiki/Special:RecentChanges", method='GET', params=filters)
            soup = BeautifulSoup(rc_resp)
            recentchanges_changes = soup.find_all("li", class_="mw-changeslist-line")

            # TODO: Not rely on get_text()
            recentchanges_changes_only_wl = list(map(lambda x: x.get_text(), list(filter(self.filter_function, recentchanges_changes))))

            filters['limit'] = len(recentchanges_changes_only_wl)
            wl_resp = request(site=self.site, uri="wiki/Special:Watchlist", method='GET', params=filters)
            soup = BeautifulSoup(wl_resp)
            watchlist_changes = soup.find_all("li", class_="mw-changeslist-line")
            watchlist_changes = list(map(lambda x: x.get_text(), watchlist_changes))

            print("{}: {}".format(key, value))
            print("Number in watchlist: {}".format(len(watchlist_changes)))
            print("Number in recent changes: {}".format(len(recentchanges_changes_only_wl)))

            wl_not_rc = [i for i in watchlist_changes if i not in recentchanges_changes_only_wl]
            rc_not_wl = [i for i in recentchanges_changes_only_wl if i not in watchlist_changes]
            print("In watchlist, not in recent changes: {}".format(wl_not_rc))
            print("In recent changes, not in watchlist: {}".format(rc_not_wl))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = WatchlistFeed(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
