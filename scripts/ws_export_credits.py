#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Investigate user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import os
import argparse
import requests
import pywikibot

from tabulate import tabulate

from pywikibot import Page, FilePage, output, User
from pywikibot.exceptions import NoPage

from bs4 import BeautifulSoup

import ebooklib
from ebooklib import epub

class WSExportCredits(object):

    """Submit Special:Investigate"""

    already_visited = []

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-f', '--file', default=[], action='append', help="EPUB file or files.")
        parser.add_argument('-d', '--dir', help="Directory of EPUB files.")
        parser.add_argument('-b', '--bot', action='store_true', help="Include bot contributors.")
        parser.add_argument('-a', '--anon', action='store_true', help="Include anonymous (i.e. IP) contributors.")
        self.options = parser.parse_args(my_args)

    def page_contributors(self, page):
        contribs = page.contributors(total=500)
        if not self.options.anon:
            contribs = [user for user in contribs if not User(self.site, user).isAnonymous()]
        if not self.options.bot:
            contribs = [user for user in contribs if 'bot' not in User(self.site, user).groups()]
        return contribs

    def image_contributors(self, image_name):
        contribs = []
        try:
            # Try to find file in local wiki
            image = FilePage(self.site, image_name)
            history = image.get_file_history()
            for timestamp, info in history.items():
                contribs.append(info['user'])
            site = self.site
        except:
            try:
                # Otherwise, file is on Commons
                commons_site = pywikibot.Site('commons', 'commons')
                image = FilePage(commons_site, image_name)
                history = image.get_file_history()
                for timestamp, info in history.items():
                    contribs.append(info['user'])
                site = commons_site
            except:
                # Not found on commons either, do nothing
                foo = "bar"

        if not self.options.anon:
            contribs = [user for user in contribs if not User(site, user).isAnonymous()]
        if not self.options.bot:
            contribs = [user for user in contribs if 'bot' not in User(site, user).groups()]

        return list(set(contribs))

    def contributors_from_api(self, page_name):
        contributors = []
        page = Page(self.site, page_name)

        # Only calculate contributors for each page once
        if page.title() not in self.already_visited:
            self.already_visited.append(page.title())

            # List of contributors of the current page on its own (no subpages, transclusions, images)
            try:
                contribs = self.page_contributors(page)
                contributors.extend(contribs)
                output("{}: {}".format(page_name, contribs))
            except:
                # Page probably does not exist, do nothing
                foo = "bar"

            # # List of contributors for all images on this page
            # for image in page.imagelinks(total=500):
            #     if image.title() not in self.already_visited:
            #         self.already_visited.append(image.title())
            #         try:
            #             # Try to find file in local wiki
            #             history = image.get_file_history()
            #             contribs = []
            #             for timestamp, info in history.items():
            #                 # We don't always appear to be able to extract the mime type of an image
            #                 if image.title().split(".")[-1] in ["jpg", "jpeg", "png", "svg"] or (hasattr(info, 'mime') and info['mime'] in ["image/svg+xml", "image/png", "image/jpeg"]):
            #                     contribs.append(info['user'])
            #                     contributors.append(info['user'])
            #             output("{}: {}".format(image.title(), contribs))
            #         except:
            #             # Otherwise, file is on Commons
            #             commons_site = pywikibot.Site('commons', 'commons')
            #             img = FilePage(commons_site, image.title(with_ns=False))
            #             history = img.get_file_history()
            #             contribs = []
            #             for timestamp, info in history.items():
            #                 # We don't always appear to be able to extract the mime type of an image
            #                 if image.title().split(".")[-1] in ["jpg", "jpeg", "png", "svg"] or (hasattr(info, 'mime') and info['mime'] in ["image/svg+xml", "image/png", "image/jpeg"]):
            #                     contribs.append(info['user'])
            #                     contributors.append(info['user'])
            #             output("{}: {}".format(img.title(), contribs))

            # We treat the Index page differently
            # for transclusion in self.site.pagetemplates(page, namespaces='Index', total=500):
            #     try:
            #         contribs = self.page_contributors(transclusion)
            #         contributors.extend(contribs)
            #         output("{}: {}".format(transclusion.title(), contribs))
            #     except:
            #         # Index page not found?
            #         foo = "bar"

            # Only those transclusions in the Page namespace
            for transclusion in self.site.pagetemplates(page, namespaces='Page', total=500):
                contributors.extend(self.contributors_from_api(transclusion.title()))

            for subpage in page.linkedPages(namespaces=0, total=500):
                # We only want subpages of current page (how to detect a subpage, I am unsure)
                if subpage.depth > page.depth and page.title() in subpage.title():
                    contributors.extend(self.contributors_from_api(subpage.title()))

        # Remove duplicate contributors
        return list(set(contributors))

    def run(self):
        files = []
        files.extend(self.options.file)
        if self.options.dir:
            dir_files = os.listdir(self.options.dir)
            files.extend([self.options.dir + "/" + f for f in dir_files])
        for file_name in files:

            output("--------------------")
            book = epub.read_epub(file_name)
            url = book.get_metadata('DC', 'source')[0][0]
            page_name = url.split("/")[-1]
            output("EBook File: {}".format(file_name))
            output("Book URL: {}".format(url))
            output("Book Title: {}".format(page_name))

            output("--------------------")
            output("Contributors ({}) for each page, subpage and transclusion".format("not including bots" if not self.options.bot else "including bots"))
            output("--------------------")
            self.already_visited = []
            api_contributors = self.contributors_from_api(page_name)

            output("--------------------")
            output("Contributors ({}) for each image in the EBook".format("not including bots" if not self.options.bot else "including bots"))
            output("--------------------")
            docs = book.get_items_of_type(ebooklib.ITEM_DOCUMENT)
            for doc in docs:
                if doc.file_name not in ["about.xhtml", "title.xhtml", "nav.xhtml"]:
                    content = doc.get_content()
                    soup = BeautifulSoup(content, features="lxml")
                    images = soup.find_all("img")
                    for image in images:
                        if 'resource' in image.attrs.keys():
                            image_name = image['resource'].split("/")[-1]
                            image_contribs = self.image_contributors(image_name)
                            # WS Export does not add credits for these images (should it?)
                            api_contributors.extend(image_contribs)
                            output("{}: {}".format(image_name, image_contribs))
                        elif 'alt' in image.attrs.keys():
                            image_name = image['alt']
                            image_contribs = self.image_contributors(image_name)
                            api_contributors.extend(image_contribs)
                            output("{}: {}".format(image_name, image_contribs))
                        elif 'href' in image.parent.attrs.keys():
                            image_href = image.parent['href']
                            image_name = image_href.split("/")[-1]
                            image_contribs = self.image_contributors(image_name)
                            api_contributors.extend(image_contribs)
                            output("{}: {}".format(image_name, image_contribs))

            api_contributors_with_index = []
            page = Page(self.site, page_name)
            # We treat the Index page differently
            for transclusion in self.site.pagetemplates(page, namespaces='Index', total=500):
                try:
                    contribs = self.page_contributors(transclusion)
                    api_contributors_with_index.extend(contribs)
                    output("{}: {}".format(transclusion.title(), contribs))
                except:
                    # Index page not found?
                    foo = "bar"

            api_contributors = list(set(api_contributors))
            api_contributors.sort()

            # Get the credits as they appear in the EPUB
            docs = book.get_items_of_type(ebooklib.ITEM_DOCUMENT)
            for doc in docs:
                if doc.file_name == "about.xhtml":
                    about = doc.get_content()
            soup = BeautifulSoup(about, features="lxml")
            contributor_div = soup.find("div", id="ws-contributor")
            epub_contributors = []
            for contributor in contributor_div.find_all("li"):
                epub_contributors.append(contributor.get_text())
            epub_contributors.sort()

            # Get the credits as they appear in Phetools
            response = requests.get("https://phetools.toolforge.org/credits.py?lang={}&format=json&page={}".format(self.site.lang, page_name))
            phetools_contributors = list(response.json().keys())
            phetools_contributors.sort()

            api_contributors_with_index = list(set(api_contributors + api_contributors_with_index))
            api_contributors_with_index.sort()

            output("--------------------")
            output("Final results")
            output("--------------------")
            output(tabulate({'EPUB': epub_contributors,
                             'API': api_contributors,
                             'API (with Index)': api_contributors_with_index,
                             'Phetools': phetools_contributors}, headers="keys"))
            output("--------------------")
            output("EPUB matched API?: {}".format(epub_contributors == api_contributors))
            # output("EPUB matched API (with Index)?: {}".format(epub_contributors == api_contributors_with_index))
            if epub_contributors != api_contributors:
                output("Contributors in EPUB not in API: {}".format(set(epub_contributors) - set(api_contributors)))
                output("Contributors in API not in EPUB: {}".format(set(api_contributors) - set(epub_contributors)))
                output("--------------------")
                output("EPUB Credits:")
                output(epub_contributors)
                output("--------------------")
                output("API Credits:")
                output(api_contributors)
                output("--------------------")
                output("API (with Index) Credits:")
                output(api_contributors_with_index)
                output("--------------------")
                output("Phetools Credits:")
                output(phetools_contributors)
            output("--------------------")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = WSExportCredits(*args)
    response = app.run()


if __name__ == '__main__':
    main()
