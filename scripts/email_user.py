#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Send an email to the specified user.

The following parameters are supported:

-u                Username of the user you want to send an email
-s                Subject of the email (defaults to "Test Subject")
-b                Body of the email (defaults to "Test Body")
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class EmailUser(object):

    """Send an email to a user."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-u', '--user', required=True)
        parser.add_argument('-s', '--subject', default="Test Subject")
        parser.add_argument('-b', '--body', default="Test Body")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        user = User(self.site, self.options.user)
        try:
            user.send_email(self.options.subject, self.options.body)
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = EmailUser(*args)
    app.run()


if __name__ == '__main__':
    main()
