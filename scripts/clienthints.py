#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# This script finds two revisions at random and compares their diff output from
# MediaWiki with the wikidiff2 demo (https://wikidiff2-demo.wmcloud.org/demo.php)
# It will produce several HTML files to allow you to review the output of the diffs.

# 1. Follow the install instructions for pywikibot:
# https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Copy this file (random_diffs_inline_two-column.py) into the pywikibot "scripts" directory

# Generate up to 10 random diffs:
# python3 pwb.py random_diffs_stress -t 10 -lang:<lang> -family:<family>

import pywikibot
import argparse
import json
import csv

from urllib.parse import quote
from datetime import datetime
from pywikibot.comms import http


class RandomDiffs(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-r', '--revid', type=int, required=True)
        parser.add_argument('-i', '--ip', default=False)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        headers = {"Content-Type": "application/json"}
        if self.options.ip:
            headers['X-Forwarded-For'] = self.options.ip
            username = self.options.ip
            self.site.logout()
        else:
            username = self.site.username()
            self.site.login()

        clienthints = {"architecture":"",
                       "bitness":"64",
                       "brands":[{"brand":"Not.A/Brand","version":"8"},{"brand":"Chromium","version":"114"}],
                       "fullVersionList":[{"brand":"Not.A/Brand","version":"8.0.0.0"},{"brand":"Chromium","version":"114.0.5735.198"}],
                       "mobile":False,
                       "model":"",
                       "platform":"Linux",
                       "platformVersion":"5.10.0"}

        rest_response = http.session.post("{}://{}{}/rest.php/checkuser/v0/useragent-clienthints/revision/{}".format(self.site.protocol(), self.site.family.langs[self.site.code],
                                                                                                                     self.site.scriptpath(), self.options.revid), json.dumps(clienthints), headers=headers).json()
        pywikibot.output(rest_response)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = RandomDiffs(*args)
    app.run()


if __name__ == '__main__':
    main()
