#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# This script simulates edits being made from random IP addresses.

# 1. Follow the install instructions for pywikibot:
# https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Copy this file (ip_edits_random.py) into the pywikibot "scripts" directory

# TODO: Document LocalSettings.php

# Generate a single edit from 5 different random IP addresses:
# python3 pwb.py ip_edits_random -lang:<lang> -family:<family>

# Generate 5 edits (per IP address) from 10 different random IP addresses:
# python3 pwb.py ip_edits_random -t 10 -p 5 -lang:<lang> -family:<family>

# Generate 5 edits (per IP address) from 10 different random IP addresses. The 5
# edits for each IP address are made sequentially:
# python3 pwb.py ip_edits_random -t 10 -p 5 -n -lang:<lang> -family:<family>


import argparse
import pywikibot
import ipaddress
import random

from pywikibot.comms import http


class IPEditsRandom(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total-ips', type=int, default=5, help="Number of IPs to simulate")
        parser.add_argument('-p', '--per-ip', type=int, default=1, help="Number of edits to make for each IP")
        parser.add_argument('-n', '--no-shuffle', action='store_true', help="Randomise the order of edits, simulating user who hops between different IPs")
        parser.add_argument('-a', '--article', default="Main_Page", help="Which article on the wiki to edit")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        ips = []
        for i in range(0, self.options.total_ips):
            # This is to ensure that, on average, half of the time this will
            # simulate an IPv4 address
            tot = random.choice([2**32, 2**128])
            ip = str(ipaddress.ip_address(random.randint(0, tot)))
            for p in range(0, self.options.per_ip):
                ips.append(ip)

        if not self.options.no_shuffle:
            random.shuffle(ips)

        for ip in ips:
            csrf = self.site.get_tokens(["csrf"]).get("csrf")
            params = {'action': 'edit',
                      # 'title': self.options.article,
                      'title': ip,
                      'appendtext': "\n\nedit",
                      'format': 'json',
                      'token': "+\\" if csrf is None else csrf}
            headers = {'X-Forwarded-For': ip}
            response = http.session.post("{}://{}{}/api.php".format(self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath()), data=params, headers=headers)
            pywikibot.output(response.json())


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = IPEditsRandom(*args)
    app.run()


if __name__ == '__main__':
    main()
