#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import json
from datetime import datetime

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError
from pywikibot.data.mysql import mysql_query

from .block import Block
from .unblock import Unblock

class BlockSystematic(object):

    """BlockSystematic user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--target', required=True)
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        outputfile = "{}_{}_{}.json".format(self.site.code, self.site.family, datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))

        self.site.login()
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0)

        target = self.options.target
        sitewides = [True, False]
        anononlys = [False]
        nocreates = [False]
        autoblocks = [False]
        noemails = [False]
        hidenames = [False]
        allowusertalks = [True]
        pages = [False, "Foobar"]
        namespaces = [False, "3"]
        reblocks = [False]

        final_results = []

        for sitewide in sitewides:
            for anononly in anononlys:
                for nocreate in nocreates:
                    for autoblock in autoblocks:
                        for noemail in noemails:
                            for hidename in hidenames:
                                for allowusertalk in allowusertalks:
                                    for namespace in namespaces:
                                        for page in pages:
                                            for reblock in reblocks:
                                                params = []
                                                if not sitewide:
                                                    params.extend(["--partial"])
                                                if page or page == "":
                                                    params.extend(["--pagerestrictions", page])
                                                if namespace or namespace == "":
                                                    params.extend(["--namespacerestrictions", namespace])
                                                if anononly:
                                                    params.extend(["--anononly"])
                                                if nocreate:
                                                    params.extend(["--nocreate"])
                                                if autoblock:
                                                    params.extend(["--autoblock"])
                                                if noemail:
                                                    params.extend(["--noemail"])
                                                if hidename and sitewide:
                                                    params.extend(["--hidename"])
                                                # if allowusertalk or (not sitewide and namespace != "3"):
                                                if allowusertalk:
                                                    params.extend(["--allowusertalk"])
                                                if reblock:
                                                    params.extend(["--reblock"])

                                                blk = Block("--target", target, *params)
                                                resp = blk.run()

                                                if type(resp) is APIError:
                                                    response = str(resp)
                                                else:
                                                    response = resp

                                                # Check the database
                                                # TODO: add a condition on timestamp, to make sure we are not just getting old blocks
                                                # rows = mysql_query("SELECT * FROM ipblocks LEFT JOIN ipblocks_restrictions ON ipblocks.ipb_id=ipblocks_restrictions.ir_ipb_id WHERE ipb_address=%s", params=[target], dbname="wiki")
                                                db = []
                                                # for row in rows:
                                                #     # Convert the byte strings into unicode strings, so they can be json serialised
                                                #     db.append({k: v.decode('utf-8') if type(v) == bytes else v for k, v in row.items()})

                                                # # Autoblocks
                                                # # TODO: add a condition on timestamp, to make sure we are not just getting old blocks
                                                # rows = mysql_query("SELECT * FROM ipblocks LEFT JOIN ipblocks_restrictions ON ipblocks.ipb_id=ipblocks_restrictions.ir_ipb_id WHERE ipb_address=%s", params=["192.168.121.180"], dbname="wiki")
                                                # for row in rows:
                                                #     # Convert the byte strings into unicode strings, so they can be json serialised
                                                #     db.append({k: v.decode('utf-8') if type(v) == bytes else v for k, v in row.items()})

                                                # Log
                                                # TODO: add a condition on timestamp, to make sure we are not just getting old logs
                                                log = ""
                                                if type(resp) is not APIError:
                                                    if hidename:
                                                        logs = self.site.logevents(logtype="suppress", user=self.site.username(), total=1)
                                                    else:
                                                        logs = self.site.logevents(logtype="block", user=self.site.username(), total=1)
                                                    for logentry in logs:
                                                        log = logentry.data

                                                final_results.append({"params": params,
                                                                      "response": response,
                                                                      "log": log,
                                                                      "db": db})

                                                Unblock("--user", target).run()

        with open(outputfile, 'a', newline='') as jsonfile:
            try: 
                json.dump(final_results, jsonfile)
            except:
                print(final_results)
                jsonfile.write(final_results)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = BlockSystematic(*args)
    app.run()


if __name__ == '__main__':
    main()
