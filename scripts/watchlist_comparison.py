#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
List recent changes.

The following parameters are supported:

TODO
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals
import argparse
import pywikibot
from .recent_changes import RecentChanges
from .watchlistdrw import Watchlist


class WatchlistComparison(object):

    """List Recent Changes"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--title', default="")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        namespace = "0|2|4|5|6|7|8|9|10|11|12|13|14|15|90|91|92|93|100|101|102|103|104|105|106|107|486|487|828|829|1198|1199|2300|2301|2302|2303|2600|5500|5501"

        recentchanges = []
        titles = self.options.title.split("|")
        if titles[0]:
            for title in titles:
                rc = RecentChanges('-l', "500", '-y', "edit|new|log|categorize|external", '-p', "title|user|timestamp", '-i', title, '-n', namespace)
                rc_resp = rc.run()
                recentchanges.extend(rc_resp['query']['recentchanges'])

        wl = Watchlist('-l', "500", '-t', "edit|new|log|categorize|external", '-p', "title|user|timestamp", '-a', '-n', namespace)
        wl_resp = wl.run()
        watchlist = wl_resp['query']['watchlist']

        print("Number in recent changes: {}".format(len(recentchanges)))
        print("Number in watchlist: {}".format(len(watchlist)))

        rc_not_wl = [i for i in recentchanges if i not in watchlist]
        wl_not_rc = [i for i in watchlist if i not in recentchanges]
        print("In recent changes, not in watchlist: {}".format(rc_not_wl))
        print("In watchlist, not in recent changes: {}".format(wl_not_rc))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = WatchlistComparison(*args)
    app.run()


if __name__ == '__main__':
    main()
