#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Get all pages on the current user's watchlist.

The following parameters are supported:

-n                Only list pages in the given namespaces. Separated by |.
-l                How many total results to return per request.
-t                Adds timestamp of when the user was last notified about the edit.
-o                Only list items not changed.
-c                Only list items changed.
-u                Username. Access a different user's watchlist.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class WatchListRaw(object):

    """Get all pages on the current user's watchlist."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-n', '--namespaces', default="", help="Only list pages in the given namespaces. Separated by |.")
        parser.add_argument('-l', '--limit', default=10, help="How many total results to return per request.")
        parser.add_argument('-t', '--timestamp', action='store_true', help="Adds timestamp of when the user was last notified about the edit.")
        parser.add_argument('-o', '--notchanged', action='store_true', help="Only list items not changed.")
        parser.add_argument('-c', '--changed', action='store_true', help="Only list items changed.")
        parser.add_argument('-u', '--username', default="", help="Username. Access a different user's watchlist.")
        parser.add_argument('-g', '--generator', action='store_true', help="Do a generator?")
        parser.add_argument('-s', '--show', default="", help="Errr, what to show in the generator?")
        parser.add_argument('-p', '--prop', default="", help="Properties to show in the generator?")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        if self.options.generator:
            params = {'action': 'query',
                      'generator': 'watchlistraw',
                      'format': 'json'}

            if self.options.show:
                params['gwrshow'] = self.options.show
            if self.options.prop:
                params['prop'] = self.options.prop

        else:
            params = {'action': 'query',
                      'list': 'watchlistraw',
                      'wrlimit': self.options.limit,
                      'format': 'json'}

            if self.options.namespaces:
                params['wrnamespace'] = self.options.namespaces

            if self.options.timestamp:
                params['wrprop'] = 'changed'

            if self.options.notchanged:
                params['wrshow'] = '!changed'
            if self.options.changed:
                params['wrshow'] = 'changed'

            if self.options.username:
                params['wrowner'] = self.options.username
                params['wrtoken'] = 'b6067a640a7535d808ab40cb09c1369ad7075a86'

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            return response
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = WatchListRaw(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
