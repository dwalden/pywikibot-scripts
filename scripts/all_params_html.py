#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2024 Dominic Walden
# Copyright (c) 2003-2021 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import argparse
import pywikibot
import json
import re
import csv
import os

from pywikibot.comms import http
from itertools import combinations
from datetime import datetime
from bs4 import BeautifulSoup
from copy import copy


class AllParamsApi(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-u', '--url', type=str, required=True)
        parser.add_argument('-s', '--size', type=int, default=2)
        parser.add_argument('-r', '--repeat', type=int, default=1)
        parser.add_argument('-d', '--duplicate', action='store_true')
        parser.add_argument('-l', '--loggedin', action='store_true')
        parser.add_argument('-q', '--queries', action='store_true')
        parser.add_argument('-c', '--comparison', action='store_true')
        parser.add_argument('-i', '--id', type=str, default=False)
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    # {'string', 'limit', 'namespace', 'title', 'expiry', 'integer', 'upload', 'password', 'timestamp', 'boolean', 'text', 'enum', 'user'}
    def random_data(self, param_type, name, value):
        if name == 'user':
            # return 'Admin'
            return '172.18.0.1'
        elif param_type == "integer" or param_type == "limit" or param_type == "namespace" or param_type[-2:] == "id":
            return 1
        elif param_type == "boolean" or param_type == "checkbox":
            return 1
        elif param_type == "timestamp":
            return "2024-06-06T12:12:12.000Z"
        elif param_type == "ip":
            return "1.2.3.4"
        elif param_type == "range" or param_type == "cidr":
            return "1.2.3.4/28"
        elif param_type == 'radio':
            return value
        else:
            return param_type

    def run(self):
        """Run the bot."""
        date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)
        if self.options.loggedin:
            self.site.login()
        else:
            self.site.logout()

        url = "{}://{}{}/index.php?title={}".format(self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath(), self.options.url)
        get_params = http.session.get(url)
        soup = BeautifulSoup(get_params.text, 'html.parser')
        body = soup.find('div', class_='mw-body-content')
        form = body.find('form')
        method = form['method']
        all_inputs = form.find_all('input')
        all_selects = form.find_all('select')
        required_params = {}
        possible_params = []

        for param in all_inputs:
            if param['type'] == 'hidden':
                required_params[param['name']] = param['value']
            else:
                possible_params.append({'name': param['name'],
                                        'value': self.random_data(param['type'], param['name'], param['value'])})

        for param in all_selects:
            for option in param.find_all('option'):
                possible_params.append({'name': param['name'],
                                        'value': option['value']})

        rows = []
        fields = ['query', 'time', 'call']
        hopefuls = combinations(possible_params, self.options.size)
        for hopeful in hopefuls:
            # Look for repeat names
            repeat = False
            names = []
            for param in hopeful:
                if param['name'] in names:
                    repeat = True
                    continue
                else:
                    names.append(param['name'])

            if repeat:
                continue

            param_data = copy(required_params)
            for param in hopeful:
                param_data[param['name']] = param['value']

            for i in range(0, self.options.repeat):
                pywikibot.output(param_data)

                if method == 'get':
                    response = http.session.get(url, params=param_data)
                else:
                    response = http.session.post(url, data=param_data)

                soup = BeautifulSoup(response.text, 'html.parser')

                if self.options.queries:
                    scripts = soup.find_all('script')
                    last_script = str(scripts[-1])
                    json_string = last_script.replace('<script>(RLQ=window.RLQ||[]).push(function(){mw.config.set(', '')
                    json_string = re.sub(r'\);.*</script>', '', json_string)
                    debug = json.loads(json_string)
                    for query in debug['debugInfo']['queries']:
                        foo = copy(param_data)
                        foo['repeat'] = i
                        foo['query'] = query['sql']
                        foo['time'] = query['time']
                        foo['call'] = query['function']
                        rows.append(foo)
                    fields = list(set(fields + list(param_data.keys())))

                if self.options.comparison:
                    dir_name = "{}_{}".format(self.options.url, date)
                    if not os.path.isdir(dir_name):
                        os.mkdir(dir_name)
                    if self.options.id:
                        content = soup.find('div', id=self.options.id)
                    else:
                        content = soup.find('div', class_='mw-body-content')
                    with open("{}/{}_{}.html".format(dir_name, '_'.join([param for param in param_data.values() if param not in required_params.values()]), i), 'w') as htmlout:
                        htmlout.write(str(content))

                pywikibot.output(response)

        if self.options.queries:
            with open("all_params_html_{}_{}.csv".format(self.site.family.langs[self.site.code], date), "a") as csvfile:
                dictcsv = csv.DictWriter(csvfile, fields, restval=None, extrasaction="ignore")
                dictcsv.writeheader()
                dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = AllParamsApi(*args)
    app.run()


if __name__ == '__main__':
    main()
