#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Block user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError
from pywikibot.data.mysql import mysql_query

from .block import Block

class BlockSave(object):

    """BlockSave user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.throttle.setDelays(0, 0)
        self.site.login()
        wgBlockAllowsUTEdit = False
        target = "Eve"
        sitewides = [True, False]
        anononlys = [True, False]
        nocreates = [True, False]
        autoblocks = [True, False]
        noemails = [True, False]
        hidenames = [True, False]
        allowusertalks = [True, False]
        page = "Foo moved"
        namespaces = ["0", "3"]

        for sitewide in sitewides:
            for anononly in anononlys:
                for nocreate in nocreates:
                    for autoblock in autoblocks:
                        for noemail in noemails:
                            for hidename in hidenames:
                                for allowusertalk in allowusertalks:
                                    for namespace in namespaces:
                                        params = []
                                        if not sitewide:
                                            params.extend(["--partial", "--pagerestrictions", page])
                                            params.extend(["--namespacerestrictions", namespace])
                                        if anononly:
                                            params.extend(["--anononly"])
                                        if nocreate:
                                            params.extend(["--nocreate"])
                                        if autoblock:
                                            params.extend(["--autoblock"])
                                        if noemail:
                                            params.extend(["--noemail"])
                                        if hidename and sitewide:
                                            params.extend(["--hidename"])
                                        if allowusertalk or (not sitewide and namespace != "3"):
                                            params.extend(["--allowusertalk"])

                                        blk = Block("--target", target, *params)
                                        blk.run()
                                        blocks = self.site.blocks(users=target)

                                        # What is the expected value of allow user talk?
                                        if not sitewide and (not namespace or namespace != "3"):
                                            # * If the UserTalk namespace is not blocked, then ipb_allow_usertalk is always true
                                            expected_allow_usertalk = True
                                        elif (sitewide or (namespace and namespace == "3")) and not wgBlockAllowsUTEdit:
                                            # * If the UserTalk namespace is blocked and $wgBlockAllowsUTEdit is false, then ipb_allow_usertalk is always false (this is the bug - it's currently always true)
                                            expected_allow_usertalk = False
                                        else:
                                            # * Otherwise, ipb_allow_usertalk is determined by the form field
                                            expected_allow_usertalk = allowusertalk

                                        # Check the API
                                        for block in blocks:
                                            if 'allowusertalk' in block:
                                                actual_allow_usertalk = True
                                            else:
                                                actual_allow_usertalk = False

                                            if expected_allow_usertalk != actual_allow_usertalk:
                                                pywikibot.output(params)
                                                pywikibot.output("Expected: {}".format(expected_allow_usertalk))
                                                pywikibot.output("Actual: {}".format(actual_allow_usertalk))

                                        # Check the database
                                        rows = mysql_query("SELECT ipb_allow_usertalk FROM ipblocks WHERE ipb_address=%s", params=[target], dbname="wiki")
                                        for row in rows:
                                            actual_allow_usertalk = bool(row[0])
                                            if expected_allow_usertalk != actual_allow_usertalk:
                                                pywikibot.output(params)
                                                pywikibot.output("Expected: {}".format(expected_allow_usertalk))
                                                pywikibot.output("Actual: {}".format(actual_allow_usertalk))

                                        self.site.unblockuser(User(self.site, target))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = BlockSave(*args)
    app.run()


if __name__ == '__main__':
    main()
