#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Edit a Wikipedia article with your favourite editor.

TODO:

 - non existing pages
 - edit conflicts
 - minor edits
 - watch/unwatch
 - ...

The following parameters are supported:

-p P              Choose which page to edit.
--page P          This argument can be passed positionally.

-w                Add the page to the user's watchlist after editing.
--watch
"""
#
# (C) Gerrit Holl, 2004
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals

import argparse
import os
import sys
import tempfile
import random
import string

import pywikibot

from pywikibot import i18n


class CreatePage(object):

    """Edit a wiki page."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()
        self.site.throttle.setDelays(0, 0)

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser(add_help=False)
        parser.add_argument('-t', '--title', required=True, type=str)
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)
        self.site.login()
        page = pywikibot.Page(pywikibot.Link(self.options.title, self.site))
        page.put("Edit by pywikibot", summary="Created by pywikibot")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = CreatePage(*args)
    app.run()


if __name__ == '__main__':
    main()
