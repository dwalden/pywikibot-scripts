#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
RandomBlock user, IP or IP range.

The following parameters are supported:

-t                Target of the block. Must be username, IP or IP range.
-e                Expiry of block.
-r                Reason string for block.
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import ipaddress
import random
import csv

import pywikibot
from pywikibot.page import User
from pywikibot.data import api
from pywikibot.data.api import Request, APIError

from .block import Block

class RandomBlock(object):

    """RandomBlock user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=10)
        parser.add_argument('-f', '--file', required=True)
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)

        with open(self.options.file, "r") as ip_file:
            lines = ip_file.read().splitlines()
            ips = random.choices(lines, k=self.options.total)

        self.site.login()

        for ip in ips:
            leading = ip.split("/")[1]
            try:
                i = random.randint(1, 2**(128-int(leading)))
                target = str(ipaddress.IPv6Network(ip)[i])
            except:
                i = random.randint(1, 2**(32-int(leading)))
                target = str(ipaddress.IPv4Network(ip)[i])

            blk = Block("--target", target)
            response = blk.run()
            pywikibot.output(response)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = RandomBlock(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
