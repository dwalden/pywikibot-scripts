#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
List recent changes.

The following parameters are supported:

TODO
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals
import argparse
import pywikibot

from datetime import datetime
from bs4 import BeautifulSoup

from .watchlistdrw import Watchlist
from .watchlist_feed import WatchlistFeed


class WatchlistFeedComparison(object):

    """List Recent Changes"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--title', default="")
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        wl = Watchlist('-l', "500", '-t', "edit|new|log|categorize|external", '-p', "title|user|timestamp", '-a', '-e', "2020-04-09T04:26:00Z", '-s', "2020-04-10T10:26:00Z")
        wl_resp = wl.run()
        watchlist = wl_resp['query']['watchlist']
        watchlist = [{'title': i['title'], 'user': i['user'], 'timestamp': i['timestamp']} for i in watchlist]

        wlf = WatchlistFeed('-i', "30", '-t', "edit|new|log|categorize|external", '-a')
        wlf_resp = wlf.run()
        soup = BeautifulSoup(wlf_resp, "xml")
        watchlistfeed = soup.find_all("item")

        watchlistfeeddict = []
        for item in watchlistfeed:
            watchlistfeeddict.append({'title': item.find("title").get_text(),
                                      'user': item.find("dc:creator").get_text(),
                                      'timestamp': datetime.strptime(item.find("pubDate").get_text(), "%a, %d %b %Y %H:%M:%S %Z").strftime("%Y-%m-%dT%H:%M:%SZ")})

        print("Number in watchlist: {}".format(len(watchlist)))
        print("Number in watchlistfeed: {}".format(len(watchlistfeeddict)))

        wlf_not_wl = [i for i in watchlistfeeddict if i not in watchlist]
        wl_not_wlf = [i for i in watchlist if i not in watchlistfeeddict]
        print("In watchlist feed, not in watchlist: {}".format(wlf_not_wl))
        print("In watchlist, not in watchlist feed: {}".format(wl_not_wlf))

        #pubDate: Fri, 10 Apr 2020 00:37:08 GMT
        #dc:creator
        #title


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = WatchlistFeedComparison(*args)
    app.run()


if __name__ == '__main__':
    main()
