#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
The following parameters are supported:

"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse
import requests
import pywikibot

import urllib.parse

class OcrRandom(object):

    """OcrRandom user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-l', '--lang', default="")
        parser.add_argument('-e', '--engine', default="google")
        parser.add_argument('-u', '--url', default="https://ocr-test.wmcloud.org")
        parser.add_argument('-t', '--total', type=int, default=10)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        # self.site.throttle.setDelays(0, 0)
        # self.site.login()

        files = self.site.randompages(total=self.options.total, namespaces="6")

        for image in files:
            pywikibot.output(image)
            image_url = urllib.parse.quote(image.get_file_url(), safe="")
            if image_url.endswith(('png', 'jpeg', 'jpg', 'gif', 'tiff', 'tif', 'webp')):
                req = "{}/api.php?image={}&lang={}&engine={}".format(self.options.url, image_url, self.options.lang, self.options.engine)
                pywikibot.output(req)
                response = requests.get(req)
                pywikibot.output(response.status_code)
            else:
                pywikibot.output("Invalid format. Skipping.")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = OcrRandom(*args)
    response = app.run()
    # pywikibot.output(response)


if __name__ == '__main__':
    main()
