#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Move or unwatch a page.

The following parameters are supported:

-f                from
-t                to
-w                watch
-e                expiry
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

import pywikibot
from pywikibot.data import api
from pywikibot.data.api import Request, APIError


class Move(object):

    """Move a page."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-f', '--frm', required=True, help="From title")
        parser.add_argument('-t', '--to', required=True, help="To title")
        parser.add_argument('-w', '--watch', default="preferences", help="nochange, preferences, unwatch, watch")
        parser.add_argument('-e', '--expiry', default="", help="watch expiry")
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0)
        params = {'action': 'move',
                  'format': 'json',
                  'from': self.options.frm,
                  'to': self.options.to,
                  'watchlist': self.options.watch,
                  'token': self.site.get_tokens(["csrf"])["csrf"]}
        if self.options.expiry:
            params['watchlistexpiry'] = self.options.expiry

        r = Request(site=self.site, parameters=params)
        try:
            response = r.submit()
            return response
        except APIError as err:
            pywikibot.output(err)
        except:
            pywikibot.output("Some other error")


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Move(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
